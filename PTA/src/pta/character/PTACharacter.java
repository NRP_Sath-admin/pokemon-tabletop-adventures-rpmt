package pta.character;

import java.util.ArrayList;

import pta.pokedex.TrainedPokemon;
import pta.util.FeaturePanel;
import pta.util.PTAClass;

public class PTACharacter {
	/* TODO: Update variable types to match those from the Character Manager Window.
	 *  
	 */
	private String name;
	private int age;
	private int level;
	private String gender;
	private String height;
	private String weight;
	private int money;
	private PTAClass class1;
	private PTAClass class2;
	private PTAClass class3;
	private PTAClass class4;
	private int str;
	private int dex;
	private int con;
	private int inte;
	private int wis;
	private int cha;
	private int hp;
	private ArrayList<TrainedPokemon> currentPokemon = new ArrayList<TrainedPokemon>();
	private ArrayList<TrainedPokemon> storedPokemon = new ArrayList<TrainedPokemon>();
	private ArrayList<String> BRMT;
	private String inv;
	private ArrayList<FeaturePanel> trainerTraits;
	private String notes;

	public String getNotes(){
		return notes;
	}
	
	public void setNotes(String n){
		this.notes = n;
	}
	
	public void addCurrentPokemon(TrainedPokemon tp) {
		currentPokemon.add(tp);
	}

	public void addStoredPokemon(TrainedPokemon tp) {
		storedPokemon.add(tp);
	}

	public int getAge() {
		return age;
	}

	public String getBRMT(int index) {
		return BRMT.get(index);
	}

	public int getCha() {
		return cha;
	}

	public PTAClass getClass1() {
		return class1;
	}

	public PTAClass getClass2() {
		return class2;
	}

	public PTAClass getClass3() {
		return class3;
	}

	public PTAClass getClass4() {
		return class4;
	}

	public int getCon() {
		return con;
	}

	public TrainedPokemon getCurrentPokemon(int i) {
		return currentPokemon.get(i);
	}

	public ArrayList<TrainedPokemon> getCurrentPokemonList() {
		return currentPokemon;
	}

	public int getDex() {
		return dex;
	}

	public String getGender() {
		return gender;
	}

	public String getHeight() {
		return height;
	}

	public int getInte() {
		return inte;
	}

	public String getInv() {
		return inv;
	}

	public int getLevel() {
		return level;
	}

	public int getMoney() {
		return money;
	}

	public String getName() {
		return name;
	}

	public ArrayList<TrainedPokemon> getStoredPokemon() {
		return storedPokemon;
	}

	public TrainedPokemon getStoredPokemon(int i) {
		return storedPokemon.get(i);
	}

	public int getStr() {
		return str;
	}

	public FeaturePanel getTrainerTraits(int i) {
		return trainerTraits.get(i);
	}

	public String getWeight() {
		return weight;
	}

	public int getWis() {
		return wis;
	}

	public void setAge(int age) {
		this.age = age;
	}
	
	public void setBRMT(ArrayList<String> bRMT) {
		BRMT = bRMT;
	}
	
	public void addBRMT(String bRMT) {
		BRMT.add(bRMT);
	}

	public void setCha(int cha) {
		this.cha = cha;
	}

	public void setClass1(PTAClass class1) {
		this.class1 = class1;
	}

	public void setClass2(PTAClass class2) {
		this.class2 = class2;
	}

	public void setClass3(PTAClass class3) {
		this.class3 = class3;
	}

	public void setClass4(PTAClass class4) {
		this.class4 = class4;
	}

	public void setCon(int con) {
		this.con = con;
	}

	public void setCurrentPokemonList(ArrayList<TrainedPokemon> currentPokemon) {
		this.currentPokemon = currentPokemon;
	}

	public void setDex(int dex) {
		this.dex = dex;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setHeight(String height) {
		this.height = height;
	}

	public void setInte(int inte) {
		this.inte = inte;
	}

	public void setInv(String inv) {
		this.inv = inv;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setMoney(int money) {
		this.money = money;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setStoredPokemon(ArrayList<TrainedPokemon> storedPokemon) {
		this.storedPokemon = storedPokemon;
	}

	public void setStr(int str) {
		this.str = str;
	}

	public void addTrainerTraits(FeaturePanel trainerTraits) {
		this.trainerTraits.add(trainerTraits);
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public void setWis(int wis) {
		this.wis = wis;
	}

	public int getHP() {
		return hp;
	}

	public void setHP(int hp) {
		this.hp = hp;
	}

}
