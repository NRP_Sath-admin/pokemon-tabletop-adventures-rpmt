package pta.character;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.apache.log4j.Level;

import net.miginfocom.swing.MigLayout;
import pta.pokedex.Pokemon;
import pta.pokedex.TrainedPokemon;
import pta.util.Features;
import pta.util.PTAFeatureList;
import pta.util.TrainedPokemonPreview;
import pta.util.PTAClass;
import javax.swing.JTextArea;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class CharacterManagerWindow {

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					CharacterManagerWindow window = new CharacterManagerWindow();
					window.frmPokemonTabletopAdventure.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private JFrame frmPokemonTabletopAdventure;
	private JTextField txfName;
	private JTextField txfBRMT_Name;
	private JComboBox<Features> cbxTraits;
	private ArrayList<TrainedPokemon> StoredPokemon = new ArrayList<TrainedPokemon>();
	private PTAFeatureList featureList;
	
	private final File Directory = new File(System.getProperty("user.home")
			+ "\\.pta\\pokedex\\database\\Trained Pokemon");
	
	private JTabbedPane tabbedPane;
	private JComboBox<String> cbxPokemon;
	private JComboBox<TrainedPokemon> cbxStoredPokemon;
	private TrainedPokemonPreview tppStoredPokemon;
	private JList<TrainedPokemon> listStoredPokemon;
	private TrainedPokemonPreview tppTeam;
	private JList<TrainedPokemon> listTeam;
	private JComboBox<String> cbxBRMT_Type;
	private JList<String> listBRMT = new JList<String>();
	private JComboBox cbxClass1;
	private JComboBox cbxClass2;
	private JComboBox cbxClass3;
	private JComboBox cbxClass4;
	private JTextField txfWeight;
	private JTextField txfHeight;
	private JSpinner spnrHP;
	private JSpinner spnrAge;
	private JComboBox<String> cbxGender;
	private JSpinner spnrLevel;
	private JSpinner spnrSTR;
	private JSpinner spnrDEX;
	private JSpinner spnrCON;
	private JSpinner spnrINT;
	private JSpinner spnrWIS;
	private JSpinner spnrCHA;
	private JSpinner spnrMoney;

	/**
	 * Create the application.
	 */
	public CharacterManagerWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		try {
			// Set System L&F
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (UnsupportedLookAndFeelException e) {
			// handle exception
		} catch (ClassNotFoundException e) {
			// handle exception
		} catch (InstantiationException e) {
			// handle exception
		} catch (IllegalAccessException e) {
			// handle exception
		}
		frmPokemonTabletopAdventure = new JFrame();
		frmPokemonTabletopAdventure.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(
						CharacterManagerWindow.class
								.getResource("/pta/img/32_pokeball.png")));
		frmPokemonTabletopAdventure
				.setTitle("Pok\u00E9mo Tabletop Adventure Trainer Manager");
		frmPokemonTabletopAdventure.setBounds(100, 100, 884, 369);
		frmPokemonTabletopAdventure
				.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmPokemonTabletopAdventure.getContentPane().setLayout(
				new MigLayout("", "[fill][grow,fill]", "[grow]"));

		JPanel panel = new JPanel();
		frmPokemonTabletopAdventure.getContentPane()
				.add(panel, "cell 0 0,grow");
		panel.setLayout(new MigLayout("", "[grow]", "[grow][grow]"));

		JPanel panel_2 = new JPanel();
		panel.add(panel_2, "cell 0 0,grow");
		panel_2.setLayout(new MigLayout(
				"",
				"[grow][left][left][grow][left][124.00,left][grow]",
				"[grow,center][grow,center][grow,center][grow,center][grow,center][grow,center]"));

		JLabel lblName = new JLabel("Name");
		panel_2.add(lblName, "cell 1 0,alignx left");

		txfName = new JTextField();
		panel_2.add(txfName, "cell 2 0,growx");
		txfName.setColumns(10);

		JLabel lblLevel = new JLabel("Level");
		panel_2.add(lblLevel, "cell 4 0,alignx left");

		spnrLevel = new JSpinner();
		spnrLevel.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0),
				null, new Integer(1)));
		panel_2.add(spnrLevel, "cell 5 0,growx");

		JLabel lblGender = new JLabel("Gender");
		panel_2.add(lblGender, "cell 1 1,alignx left");

		cbxGender = new JComboBox<String>();
		cbxGender.setModel(new DefaultComboBoxModel(new String[] { "Male",
				"Female" }));
		panel_2.add(cbxGender, "cell 2 1,growx");
		
		JLabel lblMoney = new JLabel("Money");
		panel_2.add(lblMoney, "cell 4 1");
		
		spnrMoney = new JSpinner();
		panel_2.add(spnrMoney, "cell 5 1,growx");

		JLabel lblAge = new JLabel("Age");
		panel_2.add(lblAge, "cell 1 2,alignx left");

		spnrAge = new JSpinner();
		panel_2.add(spnrAge, "cell 2 2,growx");
				
						JLabel lblClass = new JLabel("Class #1");
						panel_2.add(lblClass, "cell 4 2,alignx left");
		
				cbxClass1 = new JComboBox();
				cbxClass1.setModel(new DefaultComboBoxModel(PTAClass.values()));
				panel_2.add(cbxClass1, "cell 5 2,growx");

		JLabel lblHitPoints = new JLabel("Hit Points");
		panel_2.add(lblHitPoints, "cell 1 3,alignx left");

		JLabel lblMaxHP = new JLabel("30/");
		panel_2.add(lblMaxHP, "flowx,cell 2 3,alignx left");

		spnrHP = new JSpinner();
		panel_2.add(spnrHP, "cell 2 3,growx");
				
						JLabel lblClass_1 = new JLabel("Class #2");
						panel_2.add(lblClass_1, "cell 4 3,alignx left");
		
				cbxClass2 = new JComboBox();
				cbxClass2.setModel(new DefaultComboBoxModel(PTAClass.values()));
				panel_2.add(cbxClass2, "cell 5 3,growx");

		JLabel lblHeight = new JLabel("Height");
		panel_2.add(lblHeight, "cell 1 4,alignx left");

		txfHeight = new JTextField();
		panel_2.add(txfHeight, "cell 2 4,growx");
				
						JLabel lblClass_2 = new JLabel("Class #3");
						panel_2.add(lblClass_2, "cell 4 4,alignx left");
		
				cbxClass3 = new JComboBox();
				cbxClass3.setModel(new DefaultComboBoxModel(PTAClass.values()));
				panel_2.add(cbxClass3, "cell 5 4,growx");

		JLabel lblWeight = new JLabel("Weight");
		panel_2.add(lblWeight, "cell 1 5,alignx left");

		txfWeight = new JTextField();
		panel_2.add(txfWeight, "cell 2 5,growx");
				
						JLabel lblClass_3 = new JLabel("Class #4");
						panel_2.add(lblClass_3, "cell 4 5,alignx left");
		
				cbxClass4 = new JComboBox();
				cbxClass4.setModel(new DefaultComboBoxModel(PTAClass.values()));
				panel_2.add(cbxClass4, "cell 5 5,growx");

		JPanel panel_3 = new JPanel();
		panel.add(panel_3, "cell 0 1,grow");
		panel_3.setLayout(new MigLayout("", "[][][][][][]", "[][][]"));

		JLabel lblStr = new JLabel("STR");
		panel_3.add(lblStr, "cell 0 0");

		spnrSTR = new JSpinner();
		panel_3.add(spnrSTR, "cell 1 0");

		JLabel lblModStr = new JLabel("MOD: -4");
		panel_3.add(lblModStr, "cell 2 0");

		JLabel lblInt = new JLabel("INT");
		panel_3.add(lblInt, "cell 3 0");

		spnrINT = new JSpinner();
		panel_3.add(spnrINT, "cell 4 0");

		JLabel lblModInt = new JLabel("MOD: -4");
		panel_3.add(lblModInt, "cell 5 0");

		JLabel lblDex = new JLabel("DEX");
		panel_3.add(lblDex, "cell 0 1");

		spnrDEX = new JSpinner();
		panel_3.add(spnrDEX, "cell 1 1");

		JLabel lblModDex = new JLabel("MOD: -4");
		panel_3.add(lblModDex, "cell 2 1");

		JLabel lblWis = new JLabel("WIS");
		panel_3.add(lblWis, "cell 3 1");

		spnrWIS = new JSpinner();
		panel_3.add(spnrWIS, "cell 4 1");

		JLabel lblModWis = new JLabel("MOD: -4");
		panel_3.add(lblModWis, "cell 5 1");

		JLabel lblCon = new JLabel("CON");
		panel_3.add(lblCon, "cell 0 2");

		spnrCON = new JSpinner();
		panel_3.add(spnrCON, "cell 1 2");

		JLabel lblModCon = new JLabel("MOD: -4");
		panel_3.add(lblModCon, "cell 2 2");

		JLabel lblCha = new JLabel("CHA");
		panel_3.add(lblCha, "cell 3 2");

		spnrCHA = new JSpinner();
		panel_3.add(spnrCHA, "cell 4 2");

		JLabel lblModCha = new JLabel("MOD: -4");
		panel_3.add(lblModCha, "cell 5 2");

		tabbedPane = new JTabbedPane(SwingConstants.TOP);
		tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		frmPokemonTabletopAdventure.getContentPane().add(tabbedPane,
				"cell 1 0,grow");

		JPanel pnlPokemon = new JPanel();
		tabbedPane.addTab("Pok\u00E9mon", null, pnlPokemon, null);
		pnlPokemon.setLayout(new BorderLayout(0, 0));

		JPanel panel_9 = new JPanel();
		pnlPokemon.add(panel_9, BorderLayout.SOUTH);
		panel_9.setLayout(new MigLayout("", "[28px,grow][81px][75px]", "[23px]"));

		cbxStoredPokemon = new JComboBox<TrainedPokemon>();
		panel_9.add(cbxStoredPokemon, "cell 0 0,growx,aligny center");

		JButton btnWithdrawl = new JButton("Withdrawl");
		panel_9.add(btnWithdrawl, "cell 1 0,alignx left,aligny top");

		JButton btnDeposite = new JButton("Deposite");
		panel_9.add(btnDeposite, "cell 2 0,alignx left,aligny top");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		pnlPokemon.add(scrollPane, BorderLayout.NORTH);

		listTeam = new JList<TrainedPokemon>();
		scrollPane.setViewportView(listTeam);

		tppTeam = new TrainedPokemonPreview();
		pnlPokemon.add(tppTeam, BorderLayout.CENTER);

		JPanel pnlAwards = new JPanel();
		tabbedPane.addTab("Awards", null, pnlAwards, null);
		pnlAwards.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		pnlAwards.add(scrollPane_1, BorderLayout.CENTER);

		listBRMT = new JList<String>();
		listBRMT.setModel(new DefaultListModel<String>());
		scrollPane_1.setViewportView(listBRMT);

		JPanel panel_5 = new JPanel();
		pnlAwards.add(panel_5, BorderLayout.SOUTH);
		panel_5.setLayout(new MigLayout("", "[right][grow]", "[][][]"));

		JLabel lblName_1 = new JLabel("Name");
		panel_5.add(lblName_1, "cell 0 0,alignx trailing");

		txfBRMT_Name = new JTextField();
		panel_5.add(txfBRMT_Name, "cell 1 0,growx");
		txfBRMT_Name.setColumns(10);

		JLabel lblType = new JLabel("Type");
		panel_5.add(lblType, "cell 0 1,alignx trailing");

		cbxBRMT_Type = new JComboBox<String>();
		cbxBRMT_Type.setModel(new DefaultComboBoxModel<String>(new String[] { "Badge",
				"Ribbon", "Medal", "Trophie" }));
		panel_5.add(cbxBRMT_Type, "cell 1 1,growx");
		
		JButton btnAdd_1 = new JButton("Add");
		btnAdd_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DefaultListModel<String> temp = (DefaultListModel<String>) listBRMT.getModel();
				String val = (String)cbxBRMT_Type.getSelectedItem() + " " + txfBRMT_Name.getText();
				temp.addElement(val);
				listBRMT.setModel(temp);
			}
		});
		panel_5.add(btnAdd_1, "flowx,cell 1 2,alignx right");
		
		JButton btnRemove_1 = new JButton("Remove");
		panel_5.add(btnRemove_1, "cell 1 2,alignx right");

		JPanel pnlInventory = new JPanel();
		tabbedPane.addTab("Inventory", null, pnlInventory, null);

		JPanel pnlTraits = new JPanel();
		tabbedPane.addTab("Trainer Traits", null, pnlTraits, null);
		pnlTraits.setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		pnlTraits.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new MigLayout("", "[28px,grow][51px][71px]", "[23px]"));

		cbxTraits = new JComboBox<Features>();
		cbxTraits
				.setModel(new DefaultComboBoxModel<Features>(Features.values()));
		panel_1.add(cbxTraits, "cell 0 0,growx,aligny center");

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Features temp = (Features) cbxTraits.getSelectedItem();
				temp.getFeaturePanel().setSelected(false);
				featureList.addFeature(temp.getFeaturePanel().updateInfo());
				tabbedPane.updateUI();
			}
		});
		panel_1.add(btnAdd, "cell 1 0,alignx left,aligny top");

		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				featureList.removeSelected();
				tabbedPane.updateUI();
			}
		});
		panel_1.add(btnRemove, "cell 2 0,alignx left,aligny top");

		featureList = new PTAFeatureList();
		pnlTraits.add(featureList, BorderLayout.CENTER);

		JPanel pnlStorage = new JPanel();
		tabbedPane.addTab("Sath's Pokemon Storage System", null, pnlStorage,
				null);
		pnlStorage.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		pnlStorage.add(scrollPane_2, BorderLayout.NORTH);

		listStoredPokemon = new JList<TrainedPokemon>();
		scrollPane_2.setViewportView(listStoredPokemon);

		tppStoredPokemon = new TrainedPokemonPreview();
		pnlStorage.add(tppStoredPokemon, BorderLayout.CENTER);

		JPanel panel_10 = new JPanel();
		pnlStorage.add(panel_10, BorderLayout.SOUTH);
		panel_10.setLayout(new MigLayout("", "[grow][61px][]", "[23px]"));

		cbxPokemon = new JComboBox<String>();
		panel_10.add(cbxPokemon, "cell 0 0,growx");

		//Load pokemon data.  This is all "wild" pokemon that have not been added to you're rooster.
		File[] files = Directory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".rpm");
			}
		});
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
		if (files != null){
			try {
				for (int i = 0; i < files.length; i++)
					model.addElement(files[i].getName().substring(0,
							files[i].getName().length() - 4));
			} catch (NullPointerException e) {
				System.err.println("No files in directory!");
			} finally {
				cbxPokemon.setModel(model);
			}
		}
		JButton btnCatch = new JButton("Catch");
		btnCatch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					ObjectInputStream in = new ObjectInputStream(
							new BufferedInputStream(new FileInputStream(Directory
									.toString() + "\\" + cbxPokemon.getSelectedItem() + ".rpm")));
					TrainedPokemon tp = (TrainedPokemon) in.readObject();
					StoredPokemon.add(tp);
					in.close();
					updateLists();
				}catch(IOException e){
					e.printStackTrace();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}finally{
					DefaultComboBoxModel<TrainedPokemon> temp = new DefaultComboBoxModel<TrainedPokemon>();
					for(TrainedPokemon tp: StoredPokemon){
						temp.addElement(tp);
					}
					cbxStoredPokemon.setModel(temp);
				}
			}
		});
		panel_10.add(btnCatch, "cell 1 0,alignx left,aligny top");

		JButton btnRelease = new JButton("Release");
		panel_10.add(btnRelease, "cell 2 0");
		
		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		tabbedPane.addTab("Notes", null, scrollPane_3, null);
		
		JTextArea txaNotes = new JTextArea();
		txaNotes.setLineWrap(true);
		txaNotes.setWrapStyleWord(true);
		scrollPane_3.setViewportView(txaNotes);
		
		JMenuBar menuBar = new JMenuBar();
		frmPokemonTabletopAdventure.setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		mnFile.add(mntmSave);
		
		JMenuItem mntmLoad = new JMenuItem("Load");
		mnFile.add(mntmLoad);
	}

	private void updateLists(){
		DefaultListModel<TrainedPokemon> sp = new DefaultListModel<TrainedPokemon>();
		for(TrainedPokemon tp: StoredPokemon){
			sp.addElement(tp);
		}
		listStoredPokemon.removeAll();
		listStoredPokemon.setModel(sp);
	}
	
	private void saveCharacter(File file){
		PTACharacter saveFile = new PTACharacter();
		
		saveFile.setName(txfName.getText());
		saveFile.setAge((int)spnrAge.getValue());
		saveFile.setLevel((int)spnrLevel.getValue());
		saveFile.setGender((String)cbxGender.getSelectedItem());
		saveFile.setHeight(txfHeight.getText());
		saveFile.setWeight(txfWeight.getText());
		saveFile.setMoney((int)spnrMoney.getValue());

		for(int i = 0; i < listBRMT.getModel().getSize(); i++){
			saveFile.addBRMT(listBRMT.getModel().getElementAt(i));
		}
		
		saveFile.setClass1((PTAClass)cbxClass1.getSelectedItem());
		saveFile.setClass2((PTAClass)cbxClass2.getSelectedItem());
		saveFile.setClass3((PTAClass)cbxClass3.getSelectedItem());
		saveFile.setClass4((PTAClass)cbxClass4.getSelectedItem());
		
		saveFile.setHP((int)spnrHP.getValue());
		saveFile.setStr((int)spnrSTR.getValue());
		saveFile.setInte((int)spnrINT.getValue());
		saveFile.setDex((int)spnrDEX.getValue());
		saveFile.setWis((int)spnrWIS.getValue());
		saveFile.setCon((int)spnrCON.getValue());
		saveFile.setCha((int)spnrCHA.getValue());

		saveFile.setStoredPokemon(StoredPokemon);
		for(int i = 0; i < listStoredPokemon.getModel().getSize(); i++){
			saveFile.addCurrentPokemon(listStoredPokemon.getModel().getElementAt(i));
		}
		
	}
}
