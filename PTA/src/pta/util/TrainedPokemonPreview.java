package pta.util;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import pta.pokedex.TrainedPokemon;

public class TrainedPokemonPreview extends JPanel {
	private static final long serialVersionUID = -2623904972708038902L;
	private JLabel lblSpeciesAndName;
	private JLabel lblGender;
	private JLabel lblLevelAndExp;
	private JPanel panel;
	private JScrollPane scrollPane;
	private JTextArea txaNotes;

	public TrainedPokemonPreview() {
		setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 0, 133, 0 };
		gridBagLayout.rowHeights = new int[] { 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
		setLayout(gridBagLayout);

		panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 0, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		lblSpeciesAndName = new JLabel("<Species><Nickname>");
		GridBagConstraints gbc_lblSpeciesAndName = new GridBagConstraints();
		gbc_lblSpeciesAndName.anchor = GridBagConstraints.WEST;
		gbc_lblSpeciesAndName.insets = new Insets(0, 0, 5, 0);
		gbc_lblSpeciesAndName.gridx = 0;
		gbc_lblSpeciesAndName.gridy = 0;
		panel.add(lblSpeciesAndName, gbc_lblSpeciesAndName);

		lblGender = new JLabel("\u2642<Gender>\u2640");
		GridBagConstraints gbc_lblGender = new GridBagConstraints();
		gbc_lblGender.anchor = GridBagConstraints.WEST;
		gbc_lblGender.insets = new Insets(0, 0, 5, 0);
		gbc_lblGender.gridx = 0;
		gbc_lblGender.gridy = 1;
		panel.add(lblGender, gbc_lblGender);

		lblLevelAndExp = new JLabel("<Level(EXP)>");
		GridBagConstraints gbc_lblLevelAndExp = new GridBagConstraints();
		gbc_lblLevelAndExp.anchor = GridBagConstraints.WEST;
		gbc_lblLevelAndExp.gridx = 0;
		gbc_lblLevelAndExp.gridy = 2;
		panel.add(lblLevelAndExp, gbc_lblLevelAndExp);

		scrollPane = new JScrollPane();
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 0;
		add(scrollPane, gbc_scrollPane);

		txaNotes = new JTextArea();
		scrollPane.setViewportView(txaNotes);

	}

	public void setInfo(TrainedPokemon tp) {
		lblSpeciesAndName.setText(tp.getName() + ", AKA: " + tp.getNickName());
		lblGender.setText(tp.getGender());
		lblLevelAndExp.setText("Level: " + tp.getLevel() + "(" + tp.getExp()
				+ ")");
	}

}
