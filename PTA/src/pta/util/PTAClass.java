package pta.util;

public enum PTAClass {
	NULL_1(""),
	Ace_Trainer("Ace Trainer"),
	Chaser_Enduring_Soul("Chaser Enduring Soul"),
	Stat_Ace("Stat Ace"),
	Strategist("Strategist"),
	Tag_Battler("Tag Battler"),
	Type_Ace("Type Ace"),
	Underdog("Underdog"),
	NULL_2(""),
	
	Breeder("Breeder"),
	Botanist("Botanist"),
	Chef("Chef"),
	Evolver("Evolver"),
	Groomer("Groomer"),
	Medic("Medic"),
	Move_Tutor("Move Tutor"),
	NULL_3(""),
	
	Capture_Specialist("Capture Specialist"),
	Artificer("Artificer"),
	Collector("Collector"),
	Engineer("Engineer"),
	Juggler("Juggler"),
	Pokeball_Designer("Pokeball Designer"),
	Snagger("Snagger"),
	Trapper("Trapper"),
	
	NULL_4(""),
	Coordinator("Coordinator"),
	Beauty_Modeler("Beauty Modeler"),
	Choreographer("Choreographer"),
	Cool_Trainer("Cool Trainer"),
	Cute_Idol("Cute Idol"),
	Fashion_Designer("Fashion Designer"),
	Smart_teacher("Smart Teacher"),
	Tough_Guy("Tough Guy"),
	
	NULL_5(""),
	Martial_Artist("Martial Artist"),
	Athlete("Athlete"),
	Aura_User("Aura User"),
	Black_Belt("Black Belt"),
	Dirty_Fighter("Dirty Fighter"),
	Massage_Therapist("Message Therapist"),
	Ninja("Ninja"),
	Weapon_Master("Weapon Master"),
	NULL_6(""),
	
	Mystic("Mystic"), 
	Body_Snatcher("Body Snatcher"),
	Buffet("Buffet"),
	Godspeaker("Godspeaker"),
	Guardian("Guardian"),
	Rune_Master("Rune Master"),
	Shaman("Shaman"),
	Touched("Touched"),
	NULL_7(""),
	
	Psychic("Psychic"),
	Air_Adept("Air Adept"),
	Clairsentient("Clairsentient"),
	Earth_Shaker("Earth Shaker"),
	Empath("Empath"),
	Fire_Breather("Fire Breather"),
	Hex_Manic("Hex Manic"),
	Influential("Influential"),
	Rain_Waker("Rain_Waker"),
	NULL_8(""),
	
	Ranger("Ranger"),
	Coach("Coach"),
	Commander("Commander"),
	Detective("Detective"),
	Rider("Rider"),
	Signer("Signer"),
	Special_Operations_Member("Special Operations Member"),
	Survivalist("Durvivalist"),
	NULL_9(""),
	
	Researcher("Researcher"),
	Cryptozoologist("Cryptozoologist"),
	Dream_Doctor("Dream Doctor"),
	Petrologist("Petrologist"),
	Photographer("Photographer"),
	Professor("Professor"),
	Scientist("Scientist"),
	Watcher("Watcher");
	
	String name = "";
	
	PTAClass(String n){
		name = n;
	}
	
	public String toString(){
		return name;
	}
}
