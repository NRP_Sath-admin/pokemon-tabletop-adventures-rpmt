package pta.util;


public enum Features {
	Dual_Weilding(FeaturePanel.DualWielding),
	Aim_For_The_Horn(FeaturePanel.AimForTheHorn),
	Aim_For_The_Horn2(FeaturePanel.AimForTheHorn2),
	Back_Off(FeaturePanel.BackOff),
	Chossen_One(FeaturePanel.ChossenOne),
	Close_Your_Eyes(FeaturePanel.CloseYourEyes),
	Cover_Your_Ears(FeaturePanel.CoverYourEars),
	Give_It_Your_All(FeaturePanel.GiveItYourAll),
	Hey_Guys_Watch_This(FeaturePanel.HeyGuysWatchThis),
	Hold_Your_Breath(FeaturePanel.HoldYourBreath),
	I_Can_Take_A_Hit(FeaturePanel.ICanTakeAHit),
	I_Can_Take_A_Hit2(FeaturePanel.ICanTakeAHit2),
	I_Believe_In_You(FeaturePanel.IBelieveInYou),
	HerdOfThisPlace(FeaturePanel.HerdOfThisPlace),
	League_Member(FeaturePanel.LeagueMember),
	Let_Me_Help_You_With_That(FeaturePanel.LetMeHelpYouWithThat),
	Let_Me_Help_You_With_That2(FeaturePanel.LetMeHelpYouWithThat2),
	Get_That_Lock_Open(FeaturePanel.GetThatLockOpen),
	Look_Out(FeaturePanel.LookOut),
	Multistasking(FeaturePanel.Multitasking),
	Not_Yet(FeaturePanel.NotYet),
	Random_Knowledge(FeaturePanel.RandomKnowledge),
	Remedial_First_Aid(FeaturePanel.RemedialFirstAid),
	Satashis_Karma(FeaturePanel.SatoshisKarma),
	Satoshi_Luck(FeaturePanel.SatoshisLuck),
	Step_Aside(FeaturePanel.StepAside),
	Study_Session(FeaturePanel.StudySession),
	Voltrob_Flip(FeaturePanel.VoltrobFlip),
	What_A_Guy(FeaturePanel.WhatAGuy),
	Work_Out(FeaturePanel.WorkOut),
	Yoga_Break(FeaturePanel.YogaBreak);

	private FeaturePanel FP;

	Features(FeaturePanel fp) {
		FP = fp;
	}

	public FeaturePanel getFeaturePanel() {
		return FP;
	}
	
	public String toString(){
		return FP.getFeatureName();
	}
}
