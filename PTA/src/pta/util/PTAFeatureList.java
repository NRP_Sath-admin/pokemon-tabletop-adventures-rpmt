package pta.util;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;


public class PTAFeatureList extends JPanel {

	private static final long serialVersionUID = -5327362540386441299L;

	private ArrayList<FeaturePanel> list = new ArrayList<FeaturePanel>();
	private int selectedIndex = -1;

	private JPanel listArea;

	/**
	 * Create the listArea.
	 */
	public PTAFeatureList() {

		setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollPane);

		listArea = new JPanel();
		scrollPane.setViewportView(listArea);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 336, 0 };
		gbl_panel.rowHeights = new int[] { 131, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		listArea.setLayout(gbl_panel);

	}

	public boolean addFeature(FeaturePanel fp) {
		if (!list.contains(fp)) {
			list.add(fp);
			updateGUI();
			return true;
		} else
			return false;
	}

	public FeaturePanel getFeatureAtIndex(int index) {
		return list.get(index);
	}

	public void removeSelected() {
		listArea.removeAll();
		for (int i = list.size() - 1; i >= 0; i--) {
			System.out.println(i + " < " + list.size());
			System.out.print(list.get(i).getFeatureName());
			if (list.get(i).isSelected())
				list.remove(i);
		}
		updateGUI();
	}

	private void updateGUI() {
		for (int i = 0; i < list.size(); i++) {
			GridBagConstraints temp = new GridBagConstraints();
			temp.fill = GridBagConstraints.HORIZONTAL;
			temp.anchor = GridBagConstraints.NORTH;
			temp.gridx = 0;
			temp.gridy = i;
			listArea.add(list.get(i), temp);
		}
	}
}
