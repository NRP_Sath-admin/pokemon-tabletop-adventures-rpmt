package pta.util;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.SoftBevelBorder;

import net.miginfocom.swing.MigLayout;

public class FeaturePanel extends JPanel {
	private static final long serialVersionUID = -4485405249830493688L;

	private String name;
	MouseAdapter ma = new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			setSelected(!isSelected());
		}
	};
	private String preresiquites;
	private String frequency;
	private String tigger;
	private String effect;
	private boolean selected;
	private JLabel lblFeatureName;
	private JLabel lblPreresiquites;
	private JLabel lblFrequency;
	private JLabel lblTargettrigger;
	private JLabel lblClassFeatureCategory;
	private JTextArea txaEffect;
	private JLabel lblImages;
	/*
	 * Category types and codes. Do not separate add all together. Static (S)
	 * Free Action (F) Trainer Action (T) Interrupt (I) Extended Action (+)
	 * Non-Possession (@) Legal (L) Illegal (X)
	 */
	private String category;

	public FeaturePanel() {
		init();
	}

	public String getCategory() {
		return category;
	}

	public String getEffect() {
		return effect;
	}

	public String getFeatureName() {
		return name;
	}

	public String getFrequency() {
		return frequency;
	}

	public String getPreresiquites() {
		return preresiquites;
	}

	public String getTigger() {
		return tigger;
	}

	private void init() {

		addMouseListener(ma);

		setFeatureName("Feature Name");
		setPreresiquites("Preresiquites");
		setFrequency("Frequency");
		setTrigger("Trigger");
		setEffect("SUPPER EFFECT");
		setCategory("(S)(F)(T)(I)(+)(@)(L)(X)");

		setBorder(new SoftBevelBorder(BevelBorder.LOWERED, null, null, null,
				null));
		setLayout(new MigLayout("", "[grow][right]",
				"[fill][59.00,grow,center][grow][grow][grow][grow]"));

		lblFeatureName = new JLabel(getFeatureName());
		lblFeatureName.setFont(new Font("Tahoma", Font.BOLD, 24));
		add(lblFeatureName, "flowx,cell 0 0");

		lblImages = new JLabel();
		lblImages.setHorizontalAlignment(SwingConstants.RIGHT);
		lblImages.setLayout(new BoxLayout(lblImages, BoxLayout.X_AXIS));
		add(lblImages, "cell 0 1,grow");

		Component horizontalStrut = Box.createHorizontalStrut(20);
		add(horizontalStrut, "flowx,cell 0 2");

		lblPreresiquites = new JLabel("Preresiquites: " + getPreresiquites());
		add(lblPreresiquites, "cell 0 2,alignx left");

		Component horizontalStrut_1 = Box.createHorizontalStrut(20);
		add(horizontalStrut_1, "flowx,cell 0 3");

		lblFrequency = new JLabel(getFrequency());
		add(lblFrequency, "cell 0 3,alignx left");

		Component horizontalStrut_2 = Box.createHorizontalStrut(20);
		add(horizontalStrut_2, "flowx,cell 0 4");

		lblTargettrigger = new JLabel(getTigger());
		add(lblTargettrigger, "cell 0 4");

		Component horizontalStrut_3 = Box.createHorizontalStrut(20);
		add(horizontalStrut_3, "flowx,cell 0 5");

		txaEffect = new JTextArea("Effect: SUPPER EFFECT");
		txaEffect.setFont(new Font("Tahoma", Font.PLAIN, 11));
		txaEffect.setWrapStyleWord(true);
		txaEffect.setLineWrap(true);
		txaEffect.setEditable(false);
		txaEffect.setBackground(SystemColor.menu);
		add(txaEffect, "cell 0 5,grow");

		lblClassFeatureCategory = new JLabel(getCategory());
		lblClassFeatureCategory.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblClassFeatureCategory.setHorizontalAlignment(SwingConstants.RIGHT);
		/* FeaturePanel.class.getResource("/pta/img/FC_Extended.png") */
		add(lblClassFeatureCategory, "cell 0 0,growx");

	}

	public boolean isSelected() {
		return selected;
	}

	public FeaturePanel setCategory(String category) {
		this.category = category;
		return this;
	}

	public FeaturePanel setEffect(String effect) {
		this.effect = effect;
		return this;
	}

	public FeaturePanel setFeatureName(String name) {
		this.name = name;
		return this;
	}

	public FeaturePanel setFrequency(String frequency) {
		this.frequency = frequency;
		return this;
	}

	public FeaturePanel setPreresiquites(String preresiquites) {
		this.preresiquites = preresiquites;
		return this;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
		if (this.selected)
			lblFeatureName.setForeground(Color.ORANGE);
		else
			lblFeatureName.setForeground(Color.BLACK);
	}

	public FeaturePanel setTrigger(String tigger) {
		this.tigger = tigger;
		return this;
	}

	@Override
	public String toString() {
		return getName();
	}

	public FeaturePanel updateInfo() {
		lblFeatureName.setText(getFeatureName());
		lblPreresiquites.setText("Preresiquites: " + getPreresiquites());
		lblFrequency.setText(getFrequency());
		lblTargettrigger.setText(getTigger());
		txaEffect.setText("Effect: " + getEffect());

		if (getCategory().contains("(S)")) {
			setCategory(getCategory().replace("(S)", ""));
			JLabel temp = new JLabel();
			temp.setIcon(new ImageIcon(FeaturePanel.class
					.getResource("/pta/img/FC_Static.png")));
			lblImages.add(temp);
		}
		if (getCategory().contains("(F)")) {
			setCategory(getCategory().replace("(F)", ""));
			JLabel temp = new JLabel();
			temp.setIcon(new ImageIcon(FeaturePanel.class
					.getResource("/pta/img/FC_Free.png")));
			lblImages.add(temp);
		}
		if (getCategory().contains("(T)")) {
			setCategory(getCategory().replace("(T)", ""));
			JLabel temp = new JLabel();
			temp.setIcon(new ImageIcon(FeaturePanel.class
					.getResource("/pta/img/FC_Trainer.png")));
			lblImages.add(temp);
		}
		if (getCategory().contains("(I)")) {
			setCategory(getCategory().replace("(I)", ""));
			JLabel temp = new JLabel();
			temp.setIcon(new ImageIcon(FeaturePanel.class
					.getResource("/pta/img/FC_Interrupt.png")));
			lblImages.add(temp);
		}
		if (getCategory().contains("(+)")) {
			setCategory(getCategory().replace("(+)", ""));
			JLabel temp = new JLabel();
			temp.setIcon(new ImageIcon(FeaturePanel.class
					.getResource("/pta/img/FC_Extended.png")));
			lblImages.add(temp);
		}
		if (getCategory().contains("(@)")) {
			setCategory(getCategory().replace("(@)", ""));
			JLabel temp = new JLabel();
			temp.setIcon(new ImageIcon(FeaturePanel.class
					.getResource("/pta/img/FC_NonPossession.png")));
			lblImages.add(temp);
		}
		if (getCategory().contains("(L)")) {
			setCategory(getCategory().replace("(L)", ""));
			JLabel temp = new JLabel();
			temp.setIcon(new ImageIcon(FeaturePanel.class
					.getResource("/pta/img/FC_Legal.png")));
			lblImages.add(temp);
		}
		if (getCategory().contains("(X)")) {
			setCategory(getCategory().replace("(X)", ""));
			JLabel temp = new JLabel();
			temp.setIcon(new ImageIcon(FeaturePanel.class
					.getResource("/pta/img/FC_Ilegal.png")));
			lblImages.add(temp);
		}
		lblClassFeatureCategory.setText(getCategory());

		return this;
	}

	/*
	static FeaturePanel template = new FeaturePanel()
	.setFeatureName("")
	.setPreresiquites("")
	.setEffect("") .setFrequency("")
	.setTrigger("")
	.setCategory("");
	 */
	static FeaturePanel DualWielding = new FeaturePanel()
			.setFeatureName("Dual Wielding")
			.setPreresiquites(
					"Level 10, 12 STR, 12 CON, 12 DEX, 12 INT, 12 WIS, 12 CHA")
			.setEffect(
					"You may control 2 Pokemon at the same time. During a round of battle, they are each included in the battle queue. You are still only allotted 1 Trainer Action. During a round in which you use Dual Wielding, you may not use the effects of Multitasking.")
			.setFrequency("Static").setTrigger("")
			.setCategory("Trainer Feature (S)(L)");

	static FeaturePanel AimForTheHorn = new FeaturePanel()
			.setFeatureName("Aim For the Horn!")
			.setPreresiquites("13 WIS")
			.setEffect(
					"On a roll of 19 or 20 during Accuracy Check, your Pokemon�s attack will deal Neutral damage regardless of Immunities or Resistances.")
			.setFrequency(
					"Daily � Every 4 levels gained, you may perform this Feature another time per day.")
			.setTrigger("Target: Your Pokemon targeting with a Move.")
			.setCategory("Trainer Feature (T)(L)");

	static FeaturePanel AimForTheHorn2 = new FeaturePanel()
			.setFeatureName("Aim For The Horn! +")
			.setPreresiquites("Aim For the Horn!, 16 WIS")
			.setEffect(
					"On a roll of 16-20 during Accuracy Check, your Pokemon�s attack will deal Neutral damage regardless of immunities or resistances. This Feature replaces Aim For the Horn!")
			.setFrequency(
					"Daily � Every 2 levels gained, you may perform this Feature another time per day.")
			.setTrigger("Target: Your Pokemon targeting with a Move.")
			.setCategory("Trainer Feature (F)(L)");

	static FeaturePanel BackOff = new FeaturePanel()
			.setFeatureName("Back Off")
			.setPreresiquites("13 CHA")
			.setEffect(
					"Roll 1d20 and add your CHA modifier. If the result is higher then 10, the Trainer is intimidated and withdraws their challenge.")
			.setFrequency(
					"Daily � Every 10 levels gained, you may perform this Feature another time per day.")
			.setTrigger(
					"Trigger:A Trainer, not involved in a Gym or Competition battle, challenges you.")
			.setCategory("Trainer Trait (F)(L)(@)");

	static FeaturePanel ChossenOne = new FeaturePanel()
			.setFeatureName("Chossen One")
			.setPreresiquites("Level 16")
			.setEffect(
					"You don�t fail the check. You may not apply this to a Pokemon�s check.")
			.setFrequency("Daily")
			.setTrigger("You fail to roll high enough for any type of check.")
			.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel CloseYourEyes = new FeaturePanel()
			.setFeatureName("Close Your Eyes!")
			.setPreresiquites("13 INT")
			.setEffect(
					"Use when one of your Pokemon is targeted by one of these Moves: Attract, Astonish, Captivate, Charm, Encore, Follow Me, Flash, Glare, Hypnosis, Leer, Mean Look, Sand-Attack, Scary Face, Sleep Powder, Spore, Tail Whip, Taunt, Teeter Dance. The foe must roll +2 during Accuracy Check to hit your Pokemon.")
			.setFrequency(
					"Daily � Every 5 levels gained, you may perform this Feature another time per day")
			.setTrigger(
					"Trigger: Your Pokemon is targeted by a Move listed below.")
			.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel CoverYourEars = new FeaturePanel()
			.setFeatureName("Cover Your Ears!")
			.setPreresiquites("13 WIS")
			.setEffect(
					"Use when one of you pokemon is targeted by one of these Moves: Bug Buzz, Chatter, Grasswhistle, Growl, Hyper Voice, Metal Sound, Perish Song, Roar, Screech, Sing, Snore, Supersonic, Uproar, Yawn. The foe must roll +2 during Accuracy Check to hit your Pokemon")
			.setFrequency(
					"Daily � Every 5 levels gained, you may perform this Feature another time per day.")
			.setTrigger(
					"Trigger: Your Pokemon is targeted by a Move listed below.")
			.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel GiveItYourAll = new FeaturePanel()
	.setFeatureName("Give It Your All")
	.setPreresiquites("Level 1")
	.setEffect("Target one of your Pokemon�s Moves. For one use, it is a Critical Hit that cannot miss.") 
	.setFrequency("One Time Use Only - Once you take Give it Your All, gain one additional use per 10 levels gained")
	.setTrigger("")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel HeyGuysWatchThis = new FeaturePanel()
	.setFeatureName("Hey Guy Watch This")
	.setPreresiquites("Level 1")
	.setEffect("From now on, instead of gaining a new Feature when you level up during levels that you would gain a Feature, you gain 1 Feat Point. A Feat Point can be spent at anytime you can use a Trainer Action, and does not take a Trainer Action to use. When you spend a Feat Point, you may add any Feature to your Features whose prerequisites you meet. You may not regain any Feat Points you spend. You do not gain a Feat Point on the same level you take Hey Guys, Watch This.") 
	.setFrequency("Static")
	.setTrigger("")
	.setCategory("Trainer Trait (S)(L)");

	static FeaturePanel HoldYourBreath = new FeaturePanel()
	.setFeatureName("Hold Your Breath")
	.setPreresiquites("13 CON")
	.setEffect("Use when one of you Pokemon is targeted by one of these Moves: Muddy Water, Poison Gas, Poisonpowder, Smog, Stun Spore, Surf, Sweet Scent, Whirlpool. The foe must roll +2 during Accuracy Check to hit your Pokemon.")
	.setFrequency("Daily � Every 5 levels gained, you may perform this Feature another time per day.")
	.setTrigger("Trigger: Your Pokemon is targeted by a Move listed below.")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel ICanTakeAHit = new FeaturePanel()
	.setFeatureName("I Can Take A Hit")
	.setPreresiquites("13 CON")
	.setEffect("When taking damage from anything reduce that damage by 5. This does not reduce the cost of activating Features that require HP loss.")
	.setFrequency("Static")
	.setTrigger("")
	.setCategory("Trainer Trait (S)(L)(@)");

	static FeaturePanel ICanTakeAHit2 = new FeaturePanel()
	.setFeatureName("I Can Take A Hit +")
	.setPreresiquites("17 CON")
	.setEffect("When taking damage from anything reduce that damage by 10. This does not reduce the cost of activating Features that require HP loss. This Feature replaces I Can Take a Hit.")
	.setFrequency("Static")
	.setTrigger("")
	.setCategory("Trainer Trait (S)(L)(@)");

	static FeaturePanel IBelieveInYou = new FeaturePanel()
	.setFeatureName("I Believe In You")
	.setPreresiquites("19 CHA")
	.setEffect("Your Pokemon deals an additional 1d6 during the Move�s damage for each point you have in CHA modifier. You may not use I Believe In You! With Moves that ignore weakness, resistances and stats. This may only be used once per Move.")
	.setFrequency("Daily � Every 10 levels gained, you may perform this Feature another time per day.")
	.setTrigger("Target:Your Pokemon that just hit a target with a Move.")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel HerdOfThisPlace = new FeaturePanel()
	.setFeatureName("I've Herd Of This Place Before")
	.setPreresiquites("12 INT")
	.setEffect("Roll 1d20 and add your WIS and INT modifiers. If you roll higher than 12, you know local lore, Gym specialties, major sights, and the names of important persons related to the town or city.")
	.setFrequency("Daily")
	.setTrigger("Target: A Town or City")
	.setCategory("Trainer Trait (F)(L)");

	static FeaturePanel LeagueMember = new FeaturePanel()
	.setFeatureName("League Member")
	.setPreresiquites("12 Badges OR Medals, Level 20, facilities (20,000 for Gym facility/40,000 for Frontier facility)")
	.setEffect("You take on the responsibilities of either a Frontier Brain or a Gym Leader, and you must accept challenges at least once a week. If you lose, you must give the victor a Frontier Medal if you are a brain, or a Gym badge if you are a leader. If you have 10 Badges you may become a Gym Leader, but you do not need to choose an elemental type. If you have 10 medals you may become a Frontier Brain. You don�t need to remain in your facility�s location to accept challenges, but you do need to let those at your facilities know where you are to forward challengers. Each week you are issued 2000 (for Leaders) or 4500 (for Brains) for your services as a League Member and to create Medals/Badges, which can only be done at your facilities for 1050 (for Leaders) or 2050 (for Brains). You are only paid weekly if you accept at least 3 challenges. You may not take League Member more then once, even if you have qualified for multiple positions. Add 2 to your CHA stat.")
	.setFrequency("")
	.setTrigger("")
	.setCategory("Trainer Trait (S)(L)");

	static FeaturePanel LetMeHelpYouWithThat = new FeaturePanel()
	.setFeatureName("Let Me Help You With That")
	.setPreresiquites("")
	.setEffect("The ally has +2 added to their check. A check is made while using a Feature.")
	.setFrequency("Daily � Every 4 levels gained, you may perform this Feature another time per day.")
	.setTrigger("Target:An allied Trainer making a check.")
	.setCategory("Trainer Trait (T)(X)");

	static FeaturePanel LetMeHelpYouWithThat2 = new FeaturePanel()
	.setFeatureName("Let Me Help You With That +")
	.setPreresiquites("Let Me Help You With That")
	.setEffect("The ally has +5 added to their check. A check is made while using a Feature. ")
	.setFrequency("Daily � Every 7 levels gained, you may perform this Feature another time per day.")
	.setTrigger("Target: An allied Trainer making a check.")
	.setCategory("Trainer Trait (T)(X)");

	static FeaturePanel GetThatLockOpen = new FeaturePanel()
	.setFeatureName("Get That Lock Open")
	.setPreresiquites("15 INT")
	.setEffect("Roll 1d20 and add your INT modifier. If the result is higher then 15, the lock is unlocked and is undamaged and doesn�t appear tampered with.")
	.setFrequency("Daily � Every 5 levels gained, you may perform this Feature another time per day.")
	.setTrigger("Target: A non-computerized lock.")
	.setCategory("Trainer Trait (T)(X)");

	static FeaturePanel LookOut = new FeaturePanel()
	.setFeatureName("Look Out")
	.setPreresiquites("17 DEX")
	.setEffect("Flip a coin until you flip tails. The foe must roll 1 higher during Accuracy Check to hit your Pokemon for each heads during the coin flips.")
	.setFrequency("Daily � Every 10 levels gained, you may perform this Feature another time per day.")
	.setTrigger("Target:Your Pokemon that was targeted by a Move.")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel Multitasking = new FeaturePanel()
	.setFeatureName("Multitasking")
	.setPreresiquites("12 STR, 12 CON, 12 DEX, 12 INT, 12 WIS, 12 CHA")
	.setEffect("During encounters, you may perform 2 Trainer Actions per round. During a round in which you use Multitasking, you may not use the effects of Dual Wielding.")
	.setFrequency("Static")
	.setTrigger("")
	.setCategory("Trainer Trait (S)(L)");

	static FeaturePanel NotYet = new FeaturePanel()
	.setFeatureName("Not Yet")
	.setPreresiquites("17 CON")
	.setEffect("Before fainting, the targeted Pokemon can make one last shift and Move and then immediately faints afterwards. This cannot be used with the Move Endeavor, Explosion, Flail, Pain Split, Reversal or Selfdestruct.")
	.setFrequency("Daily")
	.setTrigger("Trigger: Your Pokemon is lowered to 0 HP or less, but not greater then -100% HP.")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel RandomKnowledge = new FeaturePanel()
	.setFeatureName("Random Knowledge")
	.setPreresiquites("14 INT or 14 WIS")
	.setEffect("Roll d20 and add your INT and WIS modifiers to the roll. If you roll higher then 13, you know about the thing you targeted. If you are targeting a Pokemon, you must have targeted the Pokemon with a Pokedex")
	.setFrequency("Daily � Every 5 levels gained, you may perform this Feature another time per day.")
	.setTrigger("Target: Anything you have line of sight to, or an idea or phrase you just heard about from any source.")
	.setCategory("Trainer Trait (F)(L)");

	static FeaturePanel RemedialFirstAid = new FeaturePanel()
	.setFeatureName("Remedial First Aid")
	.setPreresiquites("13 INT or 13 WIS")
	.setEffect("Roll 1d20 and Add your WIS or INT Modifier. Heal the target this much HP.")
	.setFrequency("Daily � Every 10 levels gained, you may perform this Feature another time per day.")
	.setTrigger("Target: A Trainer.")
	.setCategory("Trainer Trait (T)(X)");

	static FeaturePanel SatoshisKarma = new FeaturePanel()
	.setFeatureName("Satoshi's Karma")
	.setPreresiquites("Satoshi�s Luck, released 3 fully-evolved, loyal Pokemon")
	.setEffect("You may re-roll any single die. This Feature replaces, Satoshi�s Luck.")
	.setFrequency("Daily")
	.setTrigger("Target: A roll.")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel SatoshisLuck = new FeaturePanel()
	.setFeatureName("Satoshi's Luck")
	.setPreresiquites("Released a fully-evolved, loyal Pokemon")
	.setEffect("At the cost of 15 HP, you may re-roll any single die. The 15 HP may not be reduced in any way.")
	.setFrequency("Daily")
	.setTrigger("Target: A roll.")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel StepAside = new FeaturePanel()
	.setFeatureName("Step Aside")
	.setPreresiquites("level 1")
	.setEffect("Your Pokemon immediately Shifts. If it Shifts away from the targeted area, the Move or Trainer is avoided. If the target is unable to shift out of the way, it takes no damage from the Move or Trainer Attack. Only additional effects with the �Spirit Surge� keyword may activate, should the Accuracy Check permit.")
	.setFrequency("One Time Use Only � After taking the feature, you gain one additional use, per 10 levels gained afterwards.")
	.setTrigger("Trigger: Your Pokemon is targeted by a Move or Trainer Attack.")
	.setCategory("Trainer Trait ");
	
	static FeaturePanel StudySession = new FeaturePanel()
	.setFeatureName("Study Session")
	.setPreresiquites("Bought a book at least once.")
	.setEffect("You must buy a book at each Pokemart for 70 per town visited. If you don�t have 70 when you enter a town you have never visited, you lose this Feature and cannot get it back. Your Wisdom stat gains 1 point or your Intelligence stat gains 1 point. You may take the Study Session Feature multiple times. If you take this Feature multiple times, you still only need to spend 70 at a time.")
	.setFrequency("Static")
	.setTrigger("")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel VoltrobFlip = new FeaturePanel()
	.setFeatureName("Voltrob Flip")
	.setPreresiquites("12 CON or 12 INT or 12 WIS")
	.setEffect("Roll 1d20 and add either your CON, INT or WIS modifier to the roll. If the roll is 13 or higher, you begin to think of an elaborate game of Voltorb Flip. The person reading your mind cannot read anything other then the game of Voltorb Flip in your mind and all other thoughts, secrets and bits of knowledge are safe from the mind reader. If Voltorb Flip is successful, any attempts to read your mind are unsuccessful for the next hour.")
	.setFrequency("At-Will")
	.setTrigger("Trigger:A human or pokemon tries to pry into your mind to read it.")
	.setCategory("Trainer Trait (F)(L)(I)");

	static FeaturePanel WhatAGuy = new FeaturePanel()
	.setFeatureName("What A Guy")
	.setPreresiquites("Donated to a charity at least once.")
	.setEffect("ou must donate 70 to a Pokecenter per town visited. If you don�t have 70 when you enter a town you have never visited, you lose this Feature and cannot get it back. Your Charisma stat gains 1 point. You may take the What a Guy Feature multiple times. If you take this Feature multiple times, you still only need to donate 70 at a time.")
	.setFrequency("Static")
	.setTrigger("")
	.setCategory("Trainer Trait ");

	static FeaturePanel WorkOut = new FeaturePanel()
	.setFeatureName("Work Out")
	.setPreresiquites("Exercised at least once.")
	.setEffect("You must exercise at least once every 3 days. A workout session should consume at least 30 minutes of time. While exercising you lose 25 HP. If you forget to exercise at least once every 3 days you lose Workout. Your Strength stat gains 1 point or your Constitution stat gains 1 point. You may take the Workout Feature multiple times. If you take this Feature multiple times, you still only lose 25 HP.")
	.setFrequency("Static")
	.setTrigger("")
	.setCategory("Trainer Trait ");

	static FeaturePanel YogaBreak = new FeaturePanel()
	.setFeatureName("Yoha Break")
	.setPreresiquites("You practice Yoga.")
	.setEffect("You must practice yoga at least once every 3 days. A yoga session should consume at least 30 minutes of time. While exercising you lose 25 HP. If you forget to do yoga at least once every 3 days you lose Yoga Break. Your Dexterity stat gains 1 point. You may take the Yoga Break multiple times. If you take this Feature multiple times, you still only lose 25 HP.")
	.setFrequency("Static")
	.setTrigger("")
	.setCategory("Trainer Trait ");
}
