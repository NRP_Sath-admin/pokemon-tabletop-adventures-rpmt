package pta.util;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;

import pta.pokedex.TrainedPokemon;
import pta.pokedex.Type;
import net.miginfocom.swing.MigLayout;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class PokemonInfoPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7278427297571884479L;
	private JLabel lblType;
	private JLabel lblEggGroup;
	private JLabel lblHeight;
	private JLabel lblWeight;
	private JTextArea txaAbilities;
	private JSpinner spnrExp;
	private JSpinner spnrLevel;
	private JComboBox<String> cbxGender;
	private JLabel lblRatio;

	/**
	 * Create the panel.
	 */
	public PokemonInfoPanel() {
		setLayout(new MigLayout("", "[54px][391px,grow]", "[14px][14px][14px][14px][23px][20px][20px][146px]"));

		JLabel lbl1 = new JLabel("Type:");
		add(lbl1, "cell 0 0,alignx center,aligny center");

		lblType = new JLabel("None, None");
		add(lblType, "cell 1 0,alignx center,aligny center");

		JLabel lbl2 = new JLabel("Egg Group:");
		add(lbl2, "cell 0 1,alignx center,aligny center");

		lblEggGroup = new JLabel("Null, Null");
		add(lblEggGroup, "cell 1 1,alignx center,aligny center");

		JLabel lbl3 = new JLabel("Height:");
		add(lbl3, "cell 0 2,alignx center,aligny center");

		lblHeight = new JLabel("0\" 0' (null)");
		add(lblHeight, "cell 1 2,alignx center,aligny center");

		JLabel lbl4 = new JLabel("Weight:");
		add(lbl4, "cell 0 3,alignx center,aligny center");

		lblWeight = new JLabel("0lb / 0kg (0)");
		add(lblWeight, "cell 1 3,alignx center,aligny center");

		JLabel lbl5 = new JLabel("Gender:");
		add(lbl5, "cell 0 4,alignx trailing,aligny center");
		
		cbxGender = new JComboBox<String>();
		cbxGender.setModel(new DefaultComboBoxModel<String>(new String[] {"Female", "Male"}));
		add(cbxGender, "flowx,cell 1 4,alignx center");

		JLabel lbl6 = new JLabel("Level:");
		add(lbl6, "cell 0 5,alignx center,aligny center");

		spnrLevel = new JSpinner();
		add(spnrLevel, "cell 1 5,growx,aligny center");

		JLabel lbl7 = new JLabel("Exp:");
		add(lbl7, "cell 0 6,alignx center,aligny center");

		spnrExp = new JSpinner();
		add(spnrExp, "cell 1 6,growx,aligny center");

		JLabel lblAbilities = new JLabel("Abilities");
		add(lblAbilities, "cell 0 7,alignx center,aligny center");

		JScrollPane scrollPane = new JScrollPane();
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollPane, "cell 1 7,grow");

		txaAbilities = new JTextArea();
		txaAbilities.setLineWrap(true);
		txaAbilities.setWrapStyleWord(true);
		txaAbilities.setText("Basic:\r\nHigh:");
		txaAbilities.setEditable(false);
		scrollPane.setViewportView(txaAbilities);
		
		lblRatio = new JLabel("Ratio 1:1");
		add(lblRatio, "cell 1 4");

	}

	public int getEXP() {
		return (Integer) spnrExp.getValue();
	}

	public int getLevel() {
		return (Integer) spnrLevel.getValue();
	}

	public void updateInfo(TrainedPokemon pokemon) {

		if (pokemon.getTYPE()[1].compareTo(Type.None) == 1)
			lblType.setText(pokemon.getTYPE()[0] + ", " + pokemon.getTYPE()[1]);
		else
			lblType.setText("" + pokemon.getTYPE()[0]);

		if (pokemon.getEggGroup()[1].compareTo("") == 1)
			lblEggGroup.setText(pokemon.getEggGroup()[0] + ", "
					+ pokemon.getEggGroup()[1]);
		else
			lblEggGroup.setText(pokemon.getEggGroup()[0]);
		lblHeight.setText(pokemon.getHeight());
		lblWeight.setText(pokemon.getWeight());
		
		cbxGender.setSelectedItem(pokemon.getGender());
		txaAbilities.setText("Basic:\n" + pokemon.getBasicAbilities()
				+ "\n\nHigh:\n" + pokemon.getHighAbilities());
		lblRatio.setText("Ratio " + pokemon.getGenderRatio());
	}

	public String getGender() {
		return (String) cbxGender.getSelectedItem();
	}
}
