package pta.pokedex;

import java.util.ArrayList;
import java.util.UUID;

public class TrainedPokemon extends Pokemon {
	private static final long serialVersionUID = 6664079649084811149L;
	
	private UUID ID;
	private ArrayList<String> learnedMoves = new ArrayList<String>();
	private String gender = "female";
	private String notes;
	private String nickName;

	private int bonusHP = 0;
	private int bonusAtk = 0;
	private int bonusDef = 0;
	private int bonusSpAtk = 0;
	private int bonusSpDef = 0;
	private int bonusSpeed = 0;

	private int exp = 0;
	private int level = 1;

	public TrainedPokemon() {
		ID = UUID.randomUUID();
	}

	public TrainedPokemon(Pokemon pkm) {
		super(pkm);
		ID = UUID.randomUUID();
	}

	public UUID getID(){
		return this.ID;
	}
	
	public void addLearnedMoves(String learnedMoves) {
		this.learnedMoves.add(learnedMoves);
	}

	public int getBonusAtk() {
		return bonusAtk;
	}

	public int getBonusDef() {
		return bonusDef;
	}

	public int getBonusHP() {
		return bonusHP;
	}

	public int getBonusSpAtk() {
		return bonusSpAtk;
	}

	public int getBonusSpDef() {
		return bonusSpDef;
	}

	public int getBonusSpeed() {
		return bonusSpeed;
	}

	public int getExp() {
		return exp;
	}

	public String getGender() {
		return gender;
	}

	public String getLearnedMovesByIndex(int index) {
		return learnedMoves.get(index);
	}
	
	public ArrayList<String> getLearnedMoves(){
		return learnedMoves;
	}

	public int getLevel() {
		return level;
	}

	public String getNickName() {
		return nickName;
	}

	public String getNotes() {
		return notes;
	}

	public int getTotalAtk() {
		return getBaseAtk().getValue() + getBonusAtk();
	}

	public int getTotalDef() {
		return getBaseDef().getValue() + getBonusDef();
	}

	public int getTotalHP() {
		return getBaseHP().getValue() + getBonusHP();
	}

	public int getTotalSpAtk() {
		return getBaseSpAtk().getValue() + getBonusDef();
	}

	public int getTotalSpDef() {
		return getBaseSpDef().getValue() + getBonusSpDef();
	}

	public int getTotalSpeed() {
		return getBaseSpeed().getValue() + getBonusSpeed();
	}

	public void setBonusAtk(int bonusAtk) {
		this.bonusAtk = bonusAtk;
	}

	public void setBonusDef(int bonusDef) {
		this.bonusDef = bonusDef;
	}

	public void setBonusHP(int bonusHP) {
		this.bonusHP = bonusHP;
	}

	public void setBonusSpAtk(int bonusSpAtk) {
		this.bonusSpAtk = bonusSpAtk;
	}

	public void setBonusSpDef(int bonusSpDef) {
		this.bonusSpDef = bonusSpDef;
	}

	public void setBonusSpeed(int bonusSpeed) {
		this.bonusSpeed = bonusSpeed;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public void setLevel(int level) {
		this.level = level;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Override
	public String toString() {
		String reVal = "";
		if(getNickName() == "" || getNickName() == null){
			reVal = getName();
		}else{
			reVal = getNickName();
		}
		if (this.gender.compareToIgnoreCase("male") == 0)
			reVal += " \u2642 " + " lvl:" + getLevel();
		else if (this.gender.compareToIgnoreCase("female") == 0)
			reVal += " \u2640 " + " lvl:" + getLevel();
		else
			reVal += " no gender lvl:" + getLevel();
		return reVal;
	}
}
