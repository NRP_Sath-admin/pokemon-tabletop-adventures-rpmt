package pta.pokedex;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

public class PokedexWindow {

	private JFrame frmPtaPokedexJedition;

	private final File Directory = new File(System.getProperty("user.home")
			+ "\\.pta\\pokedex\\database\\");

	private JLabel lblMaleFemale;
	private JLabel lblPokemonCount;
	private JLabel lblProfile;

	private JComboBox<String> cbxDiet;
	private JComboBox<String> cbxPokemonDatabase;
	private JComboBox<String> cbxEggGroup1;
	private JComboBox<String> cbxEggGroup2;
	private JComboBox<Type> cbxType2;
	private JComboBox<Type> cbxType1;

	private JTextArea txaAbilites;
	private JTextArea txaHighAbilities;
	private JTextArea txaTMList;
	private JTextArea txaHabitat;
	private JTextArea txaEvoTree;
	private JTextArea txaLevelUpMoves;
	private JTextArea txaEggMoves;
	private JTextArea txaCapability;
	private JTextArea txaMoveTutor;
	private JTextField txfGenderRatio;

	private JTextField txfWeight;
	private JTextField txfHeight;
	private JTextField txfName;

	private JSpinner spnrHatchRate;
	private JSpinner spnrCatchRate;
	private JSpinner spnrExpDrop;
	private JSpinner spnrHP;
	private JSpinner spnrAtk;
	private JSpinner spnrDef;
	private JSpinner spnrSAtk;
	private JSpinner spnrSpeed;
	private JSpinner spnrSDef;

	static final Logger logger = Logger.getLogger(PokedexWindow.class);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		BasicConfigurator.configure();

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		logger.setLevel(Level.ALL);
		logger.info("\n----------" + dateFormat.format(date) + "----------\n"
				+ "Starting Application...\n");

		Runtime.getRuntime().addShutdownHook(new Thread() {
			@Override
			public void run() {
				logger.info("Application closing.");
			}
		});

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					PokedexWindow window = new PokedexWindow();
					window.frmPtaPokedexJedition.setVisible(true);
				} catch (Exception e) {
					logger.log(
							Level.ALL,
							"Something happened during run time, let's see what it is...",
							e);
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PokedexWindow() {
		try {
			initialize();
		} catch (Exception e) {
			logger.log(Level.ALL, "Something happened during intialization.", e);
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() throws Exception {

		try {
			// Set System L&F
			logger.info("Setting System Look and feel.");
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (UnsupportedLookAndFeelException e) {
			logger.log(Level.ERROR,
					"Something happened while setting the look and feel", e);
		} catch (ClassNotFoundException e) {
			logger.log(Level.ERROR,
					"Something happened while setting the look and feel", e);
		} catch (InstantiationException e) {
			logger.log(Level.ERROR,
					"Something happened while setting the look and feel", e);
		} catch (IllegalAccessException e) {
			logger.log(Level.ERROR,
					"Something happened while setting the look and feel", e);
		}

		// Initializing GUI
		logger.info("Initializing GUI.");

		frmPtaPokedexJedition = new JFrame();
		frmPtaPokedexJedition.setIconImage(Toolkit.getDefaultToolkit()
				.getImage(
						PokedexWindow.class
								.getResource("/pta/img/32_pokeball.png")));
		frmPtaPokedexJedition.setTitle("PTA PokeDex J-0.2 Edition");
		frmPtaPokedexJedition.setBounds(100, 100, 832, 642);
		frmPtaPokedexJedition.setMinimumSize(new Dimension(832, 642));
		frmPtaPokedexJedition.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frmPtaPokedexJedition.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Pokemon sVal = new Pokemon();

				/*
				 * Write values to Pokemon sVal.
				 */
				logger.info("Organizing save data...");
				sVal.setProfile((ImageIcon) lblProfile.getIcon());
				sVal.setAtk((int) spnrAtk.getValue());
				sVal.setHP((int) spnrHP.getValue());
				sVal.setDef((int) spnrDef.getValue());
				sVal.setSpAtk((int) spnrSAtk.getValue());
				sVal.setSpDef((int) spnrSDef.getValue());
				sVal.setSpeed((int) spnrSpeed.getValue());
				sVal.setName(txfName.getText());
				sVal.setTYPE((Type) cbxType1.getSelectedItem(),
						(Type) cbxType2.getSelectedItem());
				sVal.setHeight(txfHeight.getText());
				sVal.setWeight(txfWeight.getText());
				sVal.setGenderRatio(txfGenderRatio.getText());
				sVal.setEvolution(txaEvoTree.getText());
				sVal.setHighAbilities(txaHighAbilities.getText());
				sVal.setBasicAbilities(txaAbilites.getText());
				sVal.setCapabilities(txaCapability.getText());
				sVal.setHabitat(txaHabitat.getText());
				sVal.setEggGroup((String) cbxEggGroup1.getSelectedItem(),
						(String) cbxEggGroup2.getSelectedItem());
				sVal.setHatchRate((int) spnrHatchRate.getValue());
				sVal.setCaptureRate((int) spnrCatchRate.getValue());
				sVal.setExperienceDrop((int) spnrExpDrop.getValue());
				sVal.setDiet((String) cbxDiet.getSelectedItem());
				sVal.setMoveTutor(txaMoveTutor.getText());
				sVal.setEggMoves(txaEggMoves.getText());
				sVal.setLevelUpMoves(txaLevelUpMoves.getText());
				sVal.setTMList(txaTMList.getText());

				try {
					if (Directory.exists())
						logger.info("Directory exists, no need to make it again.");
					else {
						logger.info("Creating directory "
								+ Directory.toString());
						Directory.mkdirs();
					}
					logger.info("Initiating ObjectIutputStream.");
					ObjectOutputStream out = new ObjectOutputStream(
							new BufferedOutputStream(new FileOutputStream(
									Directory.toString() + "\\"
											+ sVal.getName() + ".rpm", false)));
					logger.info("Writting save data to file.");
					out.writeObject(sVal);
					out.close();
					JOptionPane.showMessageDialog(frmPtaPokedexJedition,
							"Pokemon " + sVal.getName()
									+ " saved without any issues!",
							"Pokemon Saved", JOptionPane.INFORMATION_MESSAGE);
				} catch (IOException e) {
					logger.log(Level.ERROR, "Error while saving!", e);
					JOptionPane.showMessageDialog(frmPtaPokedexJedition,
							e.toString(), "IOException",
							JOptionPane.ERROR_MESSAGE);
					e.printStackTrace();
				}
			}
		});
		mnFile.add(mntmSave);

		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);

		JMenuItem mntmIssueTracker = new JMenuItem("Issue Tracker");
		mnHelp.add(mntmIssueTracker);

		JMenuItem mntmLogtxt = new JMenuItem("log.txt");
		mnHelp.add(mntmLogtxt);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 255, 0, 0, 0, 0, 0 };
		gridBagLayout.rowHeights = new int[] { 255, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		frmPtaPokedexJedition.getContentPane().setLayout(gridBagLayout);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frmPtaPokedexJedition.getContentPane().add(panel, gbc_panel);
		panel.setLayout(new BorderLayout(0, 0));

		lblProfile = new JLabel("<Click to load image>");
		lblProfile.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				logger.info("Creating File Chooser...");
				JFrame frame = new JFrame("Load Image");
				frame.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
				JFileChooser jfc = new JFileChooser();
				int reVal = jfc.showOpenDialog(frmPtaPokedexJedition);
				if (reVal == JFileChooser.APPROVE_OPTION) {
					BufferedImage bi;
					try {
						logger.info("Attempting to load image.");
						bi = ImageIO.read(jfc.getSelectedFile());
						ImageIcon ii = new ImageIcon(bi.getScaledInstance(240,
								240, Image.SCALE_SMOOTH));
						lblProfile.setText("");
						lblProfile.setIcon(ii);
					} catch (IOException e) {
						e.printStackTrace();
						logger.log(Level.ERROR, "Error while loading image!", e);
					}
				}
			}
		});
		lblProfile.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblProfile, BorderLayout.CENTER);

		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 5, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 1;
		gbc_panel_2.gridy = 0;
		frmPtaPokedexJedition.getContentPane().add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 0, 0 };
		gbl_panel_2.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_2.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);

		JLabel lblBasicAbilities = new JLabel("Basic Abilities");
		GridBagConstraints gbc_lblBasicAbilities = new GridBagConstraints();
		gbc_lblBasicAbilities.insets = new Insets(0, 0, 5, 0);
		gbc_lblBasicAbilities.gridx = 0;
		gbc_lblBasicAbilities.gridy = 0;
		panel_2.add(lblBasicAbilities, gbc_lblBasicAbilities);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 1;
		panel_2.add(scrollPane, gbc_scrollPane);

		txaAbilites = new JTextArea();
		scrollPane.setViewportView(txaAbilites);

		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 5);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 2;
		gbc_panel_3.gridy = 0;
		frmPtaPokedexJedition.getContentPane().add(panel_3, gbc_panel_3);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 0, 0 };
		gbl_panel_3.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_3.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		JLabel lblHighAbilities = new JLabel("High Abilities");
		GridBagConstraints gbc_lblHighAbilities = new GridBagConstraints();
		gbc_lblHighAbilities.insets = new Insets(0, 0, 5, 0);
		gbc_lblHighAbilities.gridx = 0;
		gbc_lblHighAbilities.gridy = 0;
		panel_3.add(lblHighAbilities, gbc_lblHighAbilities);

		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 1;
		panel_3.add(scrollPane_1, gbc_scrollPane_1);

		txaHighAbilities = new JTextArea();
		scrollPane_1.setViewportView(txaHighAbilities);

		JPanel panel_6 = new JPanel();
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.insets = new Insets(0, 0, 5, 5);
		gbc_panel_6.fill = GridBagConstraints.BOTH;
		gbc_panel_6.gridx = 3;
		gbc_panel_6.gridy = 0;
		frmPtaPokedexJedition.getContentPane().add(panel_6, gbc_panel_6);
		GridBagLayout gbl_panel_6 = new GridBagLayout();
		gbl_panel_6.columnWidths = new int[] { 0, 0 };
		gbl_panel_6.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_6.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_6.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel_6.setLayout(gbl_panel_6);

		JLabel lblTmList = new JLabel("TM List");
		GridBagConstraints gbc_lblTmList = new GridBagConstraints();
		gbc_lblTmList.insets = new Insets(0, 0, 5, 0);
		gbc_lblTmList.gridx = 0;
		gbc_lblTmList.gridy = 0;
		panel_6.add(lblTmList, gbc_lblTmList);

		JScrollPane scrollPane_6 = new JScrollPane();
		scrollPane_6
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane_6 = new GridBagConstraints();
		gbc_scrollPane_6.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_6.gridx = 0;
		gbc_scrollPane_6.gridy = 1;
		panel_6.add(scrollPane_6, gbc_scrollPane_6);

		txaTMList = new JTextArea();
		scrollPane_6.setViewportView(txaTMList);

		JPanel panel_8 = new JPanel();
		GridBagConstraints gbc_panel_8 = new GridBagConstraints();
		gbc_panel_8.insets = new Insets(0, 0, 5, 0);
		gbc_panel_8.fill = GridBagConstraints.BOTH;
		gbc_panel_8.gridx = 4;
		gbc_panel_8.gridy = 0;
		frmPtaPokedexJedition.getContentPane().add(panel_8, gbc_panel_8);
		GridBagLayout gbl_panel_8 = new GridBagLayout();
		gbl_panel_8.columnWidths = new int[] { 0, 0 };
		gbl_panel_8.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_8.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_8.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		panel_8.setLayout(gbl_panel_8);

		JLabel lblDatabase = new JLabel("Database");
		GridBagConstraints gbc_lblDatabase = new GridBagConstraints();
		gbc_lblDatabase.insets = new Insets(0, 0, 5, 0);
		gbc_lblDatabase.gridx = 0;
		gbc_lblDatabase.gridy = 0;
		panel_8.add(lblDatabase, gbc_lblDatabase);

		lblPokemonCount = new JLabel("Pokemon: 0");
		GridBagConstraints gbc_lblPokemonCount = new GridBagConstraints();
		gbc_lblPokemonCount.insets = new Insets(0, 0, 5, 0);
		gbc_lblPokemonCount.anchor = GridBagConstraints.WEST;
		gbc_lblPokemonCount.gridx = 0;
		gbc_lblPokemonCount.gridy = 1;
		panel_8.add(lblPokemonCount, gbc_lblPokemonCount);

		cbxPokemonDatabase = new JComboBox<String>();
		GridBagConstraints gbc_cbxPokemonDatabase = new GridBagConstraints();
		gbc_cbxPokemonDatabase.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbxPokemonDatabase.insets = new Insets(0, 0, 5, 0);
		gbc_cbxPokemonDatabase.gridx = 0;
		gbc_cbxPokemonDatabase.gridy = 2;
		panel_8.add(cbxPokemonDatabase, gbc_cbxPokemonDatabase);

		JButton btnLoad = new JButton("Load");
		btnLoad.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				loadPokemon(new File(Directory.toString() + "\\"
						+ cbxPokemonDatabase.getSelectedItem() + ".rpm"));
			}
		});
		GridBagConstraints gbc_btnLoad = new GridBagConstraints();
		gbc_btnLoad.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnLoad.insets = new Insets(0, 0, 5, 0);
		gbc_btnLoad.gridx = 0;
		gbc_btnLoad.gridy = 3;
		panel_8.add(btnLoad, gbc_btnLoad);

		JLabel lblStats = new JLabel("Stats");
		GridBagConstraints gbc_lblStats = new GridBagConstraints();
		gbc_lblStats.insets = new Insets(0, 0, 5, 0);
		gbc_lblStats.gridx = 0;
		gbc_lblStats.gridy = 4;
		panel_8.add(lblStats, gbc_lblStats);

		JPanel panel_10 = new JPanel();
		GridBagConstraints gbc_panel_10 = new GridBagConstraints();
		gbc_panel_10.fill = GridBagConstraints.BOTH;
		gbc_panel_10.gridx = 0;
		gbc_panel_10.gridy = 5;
		panel_8.add(panel_10, gbc_panel_10);
		GridBagLayout gbl_panel_10 = new GridBagLayout();
		gbl_panel_10.columnWidths = new int[] { 0, 0, 0 };
		gbl_panel_10.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_10.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_panel_10.rowWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		panel_10.setLayout(gbl_panel_10);

		JLabel lblHp = new JLabel("HP");
		GridBagConstraints gbc_lblHp = new GridBagConstraints();
		gbc_lblHp.insets = new Insets(0, 0, 5, 5);
		gbc_lblHp.gridx = 0;
		gbc_lblHp.gridy = 0;
		panel_10.add(lblHp, gbc_lblHp);

		spnrHP = new JSpinner();
		spnrHP.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1),
				null, new Integer(1)));
		GridBagConstraints gbc_spnrHP = new GridBagConstraints();
		gbc_spnrHP.insets = new Insets(0, 0, 5, 0);
		gbc_spnrHP.gridx = 1;
		gbc_spnrHP.gridy = 0;
		panel_10.add(spnrHP, gbc_spnrHP);

		JLabel lblAtk = new JLabel("Atk");
		GridBagConstraints gbc_lblAtk = new GridBagConstraints();
		gbc_lblAtk.insets = new Insets(0, 0, 5, 5);
		gbc_lblAtk.gridx = 0;
		gbc_lblAtk.gridy = 1;
		panel_10.add(lblAtk, gbc_lblAtk);

		spnrAtk = new JSpinner();
		spnrAtk.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1),
				null, new Integer(1)));
		GridBagConstraints gbc_spnrAtk = new GridBagConstraints();
		gbc_spnrAtk.insets = new Insets(0, 0, 5, 0);
		gbc_spnrAtk.gridx = 1;
		gbc_spnrAtk.gridy = 1;
		panel_10.add(spnrAtk, gbc_spnrAtk);

		JLabel lblDef = new JLabel("Def");
		GridBagConstraints gbc_lblDef = new GridBagConstraints();
		gbc_lblDef.insets = new Insets(0, 0, 5, 5);
		gbc_lblDef.gridx = 0;
		gbc_lblDef.gridy = 2;
		panel_10.add(lblDef, gbc_lblDef);

		spnrDef = new JSpinner();
		spnrDef.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1),
				null, new Integer(1)));
		GridBagConstraints gbc_spnrDef = new GridBagConstraints();
		gbc_spnrDef.insets = new Insets(0, 0, 5, 0);
		gbc_spnrDef.gridx = 1;
		gbc_spnrDef.gridy = 2;
		panel_10.add(spnrDef, gbc_spnrDef);

		JLabel lblSpAtk = new JLabel("Sp. Atk");
		GridBagConstraints gbc_lblSpAtk = new GridBagConstraints();
		gbc_lblSpAtk.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpAtk.gridx = 0;
		gbc_lblSpAtk.gridy = 3;
		panel_10.add(lblSpAtk, gbc_lblSpAtk);

		spnrSAtk = new JSpinner();
		spnrSAtk.setModel(new SpinnerNumberModel(new Integer(1),
				new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spnrSAtk = new GridBagConstraints();
		gbc_spnrSAtk.insets = new Insets(0, 0, 5, 0);
		gbc_spnrSAtk.gridx = 1;
		gbc_spnrSAtk.gridy = 3;
		panel_10.add(spnrSAtk, gbc_spnrSAtk);

		JLabel lblSpDef = new JLabel("Sp. Def");
		GridBagConstraints gbc_lblSpDef = new GridBagConstraints();
		gbc_lblSpDef.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpDef.gridx = 0;
		gbc_lblSpDef.gridy = 4;
		panel_10.add(lblSpDef, gbc_lblSpDef);

		spnrSDef = new JSpinner();
		spnrSDef.setModel(new SpinnerNumberModel(new Integer(1),
				new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spnrSDef = new GridBagConstraints();
		gbc_spnrSDef.insets = new Insets(0, 0, 5, 0);
		gbc_spnrSDef.gridx = 1;
		gbc_spnrSDef.gridy = 4;
		panel_10.add(spnrSDef, gbc_spnrSDef);

		JLabel lblSpeed = new JLabel("Speed");
		GridBagConstraints gbc_lblSpeed = new GridBagConstraints();
		gbc_lblSpeed.insets = new Insets(0, 0, 0, 5);
		gbc_lblSpeed.gridx = 0;
		gbc_lblSpeed.gridy = 5;
		panel_10.add(lblSpeed, gbc_lblSpeed);

		spnrSpeed = new JSpinner();
		spnrSpeed.setModel(new SpinnerNumberModel(new Integer(1),
				new Integer(1), null, new Integer(1)));
		GridBagConstraints gbc_spnrSpeed = new GridBagConstraints();
		gbc_spnrSpeed.gridx = 1;
		gbc_spnrSpeed.gridy = 5;
		panel_10.add(spnrSpeed, gbc_spnrSpeed);

		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 0, 5);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 0;
		gbc_panel_1.gridy = 1;
		frmPtaPokedexJedition.getContentPane().add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 68, 86, 0 };
		gbl_panel_1.rowHeights = new int[] { 20, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
				0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JLabel lblName = new JLabel("Name");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		panel_1.add(lblName, gbc_lblName);

		txfName = new JTextField();
		GridBagConstraints gbc_txfName = new GridBagConstraints();
		gbc_txfName.insets = new Insets(0, 0, 5, 0);
		gbc_txfName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txfName.anchor = GridBagConstraints.NORTH;
		gbc_txfName.gridx = 1;
		gbc_txfName.gridy = 0;
		panel_1.add(txfName, gbc_txfName);
		txfName.setColumns(10);

		JLabel lblType = new JLabel("Type");
		GridBagConstraints gbc_lblType = new GridBagConstraints();
		gbc_lblType.insets = new Insets(0, 0, 5, 5);
		gbc_lblType.gridx = 0;
		gbc_lblType.gridy = 1;
		panel_1.add(lblType, gbc_lblType);

		cbxType1 = new JComboBox<Type>();
		cbxType1.setModel(new DefaultComboBoxModel<Type>(Type.values()));
		GridBagConstraints gbc_cbxType1 = new GridBagConstraints();
		gbc_cbxType1.anchor = GridBagConstraints.WEST;
		gbc_cbxType1.insets = new Insets(0, 0, 5, 0);
		gbc_cbxType1.gridx = 1;
		gbc_cbxType1.gridy = 1;
		panel_1.add(cbxType1, gbc_cbxType1);

		cbxType2 = new JComboBox<Type>();
		cbxType2.setModel(new DefaultComboBoxModel<Type>(Type.values()));
		GridBagConstraints gbc_cbxType2 = new GridBagConstraints();
		gbc_cbxType2.anchor = GridBagConstraints.WEST;
		gbc_cbxType2.insets = new Insets(0, 0, 5, 0);
		gbc_cbxType2.gridx = 1;
		gbc_cbxType2.gridy = 2;
		panel_1.add(cbxType2, gbc_cbxType2);

		JLabel lblEggGroup = new JLabel("Egg Group");
		GridBagConstraints gbc_lblEggGroup = new GridBagConstraints();
		gbc_lblEggGroup.anchor = GridBagConstraints.EAST;
		gbc_lblEggGroup.insets = new Insets(0, 0, 5, 5);
		gbc_lblEggGroup.gridx = 0;
		gbc_lblEggGroup.gridy = 3;
		panel_1.add(lblEggGroup, gbc_lblEggGroup);

		cbxEggGroup1 = new JComboBox<String>();
		cbxEggGroup1.setModel(new DefaultComboBoxModel(new String[] { "",
				"Amorphous", "Bug", "Ditto", "Dragon", "Fairy", "Field",
				"Flying", "Gender Unknown", "Grass", "Human-Like", "Mineral",
				"Monster", "Undiscovered", "Water 1", "Water 2", "Water 3" }));
		GridBagConstraints gbc_cbxEggGroup1 = new GridBagConstraints();
		gbc_cbxEggGroup1.anchor = GridBagConstraints.WEST;
		gbc_cbxEggGroup1.insets = new Insets(0, 0, 5, 0);
		gbc_cbxEggGroup1.gridx = 1;
		gbc_cbxEggGroup1.gridy = 3;
		panel_1.add(cbxEggGroup1, gbc_cbxEggGroup1);

		cbxEggGroup2 = new JComboBox<String>();
		cbxEggGroup2.setModel(new DefaultComboBoxModel(new String[] { "",
				"Amorphous", "Bug", "Ditto", "Dragon", "Fairy", "Field",
				"Flying", "Gender Unknown", "Grass", "Human-Like", "Mineral",
				"Monster", "Undiscovered", "Water 1", "Water 2", "Water 3" }));
		GridBagConstraints gbc_cbxEggGroup2 = new GridBagConstraints();
		gbc_cbxEggGroup2.anchor = GridBagConstraints.WEST;
		gbc_cbxEggGroup2.insets = new Insets(0, 0, 5, 0);
		gbc_cbxEggGroup2.gridx = 1;
		gbc_cbxEggGroup2.gridy = 4;

		panel_1.add(cbxEggGroup2, gbc_cbxEggGroup2);

		JLabel lblHeight = new JLabel("Height");
		GridBagConstraints gbc_lblHeight = new GridBagConstraints();
		gbc_lblHeight.insets = new Insets(0, 0, 5, 5);
		gbc_lblHeight.gridx = 0;
		gbc_lblHeight.gridy = 5;
		panel_1.add(lblHeight, gbc_lblHeight);

		txfHeight = new JTextField();
		GridBagConstraints gbc_txfHeight = new GridBagConstraints();
		gbc_txfHeight.anchor = GridBagConstraints.NORTH;
		gbc_txfHeight.insets = new Insets(0, 0, 5, 0);
		gbc_txfHeight.fill = GridBagConstraints.HORIZONTAL;
		gbc_txfHeight.gridx = 1;
		gbc_txfHeight.gridy = 5;
		panel_1.add(txfHeight, gbc_txfHeight);
		txfHeight.setColumns(10);

		JLabel lblWeight = new JLabel("Weight");
		GridBagConstraints gbc_lblWeight = new GridBagConstraints();
		gbc_lblWeight.insets = new Insets(0, 0, 5, 5);
		gbc_lblWeight.gridx = 0;
		gbc_lblWeight.gridy = 6;
		panel_1.add(lblWeight, gbc_lblWeight);

		txfWeight = new JTextField();
		GridBagConstraints gbc_txfWeight = new GridBagConstraints();
		gbc_txfWeight.insets = new Insets(0, 0, 5, 0);
		gbc_txfWeight.fill = GridBagConstraints.HORIZONTAL;
		gbc_txfWeight.gridx = 1;
		gbc_txfWeight.gridy = 6;
		panel_1.add(txfWeight, gbc_txfWeight);
		txfWeight.setColumns(10);

		JLabel lblGenderRatio = new JLabel("Gender Ratio");
		GridBagConstraints gbc_lblGenderRatio = new GridBagConstraints();
		gbc_lblGenderRatio.insets = new Insets(0, 0, 5, 5);
		gbc_lblGenderRatio.gridx = 0;
		gbc_lblGenderRatio.gridy = 7;
		panel_1.add(lblGenderRatio, gbc_lblGenderRatio);

		txfGenderRatio = new JTextField();
		txfGenderRatio.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				UpdateGenderRatio();
			}
		});
		txfGenderRatio.setText("1:1");
		GridBagConstraints gbc_txtrGenderRatio = new GridBagConstraints();
		gbc_txtrGenderRatio.insets = new Insets(0, 0, 5, 0);
		gbc_txtrGenderRatio.fill = GridBagConstraints.BOTH;
		gbc_txtrGenderRatio.gridx = 1;
		gbc_txtrGenderRatio.gridy = 7;
		panel_1.add(txfGenderRatio, gbc_txtrGenderRatio);

		lblMaleFemale = new JLabel("Male: 50% / Female 50%");
		GridBagConstraints gbc_lblMaleFemale = new GridBagConstraints();
		gbc_lblMaleFemale.anchor = GridBagConstraints.WEST;
		gbc_lblMaleFemale.insets = new Insets(0, 0, 5, 0);
		gbc_lblMaleFemale.gridx = 1;
		gbc_lblMaleFemale.gridy = 8;
		panel_1.add(lblMaleFemale, gbc_lblMaleFemale);

		JLabel lblDiet = new JLabel("Diet");
		GridBagConstraints gbc_lblDiet = new GridBagConstraints();
		gbc_lblDiet.anchor = GridBagConstraints.EAST;
		gbc_lblDiet.insets = new Insets(0, 0, 5, 5);
		gbc_lblDiet.gridx = 0;
		gbc_lblDiet.gridy = 9;
		panel_1.add(lblDiet, gbc_lblDiet);

		cbxDiet = new JComboBox<String>();
		cbxDiet.setModel(new DefaultComboBoxModel<String>(new String[] {
				"Carnivore", "Herbivore", "Omnivore", "Phototroph",
				"Terravore", "Nullivore" }));
		GridBagConstraints gbc_cbxDiet = new GridBagConstraints();
		gbc_cbxDiet.anchor = GridBagConstraints.WEST;
		gbc_cbxDiet.insets = new Insets(0, 0, 5, 0);
		gbc_cbxDiet.gridx = 1;
		gbc_cbxDiet.gridy = 9;
		panel_1.add(cbxDiet, gbc_cbxDiet);

		JLabel lblHatchRate = new JLabel("Hatch Rate");
		GridBagConstraints gbc_lblHatchRate = new GridBagConstraints();
		gbc_lblHatchRate.insets = new Insets(0, 0, 5, 5);
		gbc_lblHatchRate.gridx = 0;
		gbc_lblHatchRate.gridy = 10;
		panel_1.add(lblHatchRate, gbc_lblHatchRate);

		spnrHatchRate = new JSpinner();
		spnrHatchRate.setModel(new SpinnerNumberModel(new Integer(0),
				new Integer(0), null, new Integer(1)));
		GridBagConstraints gbc_spnrHatchRate = new GridBagConstraints();
		gbc_spnrHatchRate.fill = GridBagConstraints.HORIZONTAL;
		gbc_spnrHatchRate.insets = new Insets(0, 0, 5, 0);
		gbc_spnrHatchRate.gridx = 1;
		gbc_spnrHatchRate.gridy = 10;
		panel_1.add(spnrHatchRate, gbc_spnrHatchRate);

		JLabel lblCaptureRate = new JLabel("Capture Rate");
		GridBagConstraints gbc_lblCaptureRate = new GridBagConstraints();
		gbc_lblCaptureRate.insets = new Insets(0, 0, 5, 5);
		gbc_lblCaptureRate.gridx = 0;
		gbc_lblCaptureRate.gridy = 11;
		panel_1.add(lblCaptureRate, gbc_lblCaptureRate);

		spnrCatchRate = new JSpinner();
		spnrCatchRate.setModel(new SpinnerNumberModel(new Integer(0),
				new Integer(0), null, new Integer(1)));
		GridBagConstraints gbc_spnrCatchRate = new GridBagConstraints();
		gbc_spnrCatchRate.fill = GridBagConstraints.HORIZONTAL;
		gbc_spnrCatchRate.insets = new Insets(0, 0, 5, 0);
		gbc_spnrCatchRate.gridx = 1;
		gbc_spnrCatchRate.gridy = 11;
		panel_1.add(spnrCatchRate, gbc_spnrCatchRate);

		JLabel lblExperianceDrop = new JLabel("Experiance Drop");
		GridBagConstraints gbc_lblExperianceDrop = new GridBagConstraints();
		gbc_lblExperianceDrop.insets = new Insets(0, 0, 0, 5);
		gbc_lblExperianceDrop.gridx = 0;
		gbc_lblExperianceDrop.gridy = 12;
		panel_1.add(lblExperianceDrop, gbc_lblExperianceDrop);

		spnrExpDrop = new JSpinner();
		spnrExpDrop.setModel(new SpinnerNumberModel(new Integer(0),
				new Integer(0), null, new Integer(1)));
		GridBagConstraints gbc_spnrExpDrop = new GridBagConstraints();
		gbc_spnrExpDrop.fill = GridBagConstraints.HORIZONTAL;
		gbc_spnrExpDrop.gridx = 1;
		gbc_spnrExpDrop.gridy = 12;
		panel_1.add(spnrExpDrop, gbc_spnrExpDrop);

		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 0, 5);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 1;
		gbc_panel_4.gridy = 1;
		frmPtaPokedexJedition.getContentPane().add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 0, 0 };
		gbl_panel_4.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_4.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_4.rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0,
				Double.MIN_VALUE };
		panel_4.setLayout(gbl_panel_4);

		JLabel lblHabitat = new JLabel("Habitat");
		GridBagConstraints gbc_lblHabitat = new GridBagConstraints();
		gbc_lblHabitat.insets = new Insets(0, 0, 5, 0);
		gbc_lblHabitat.gridx = 0;
		gbc_lblHabitat.gridy = 0;
		panel_4.add(lblHabitat, gbc_lblHabitat);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.gridx = 0;
		gbc_scrollPane_2.gridy = 1;
		panel_4.add(scrollPane_2, gbc_scrollPane_2);

		txaHabitat = new JTextArea();
		scrollPane_2.setViewportView(txaHabitat);

		JLabel lblEggMoves = new JLabel("Egg Moves");
		GridBagConstraints gbc_lblEggMoves = new GridBagConstraints();
		gbc_lblEggMoves.insets = new Insets(0, 0, 5, 0);
		gbc_lblEggMoves.gridx = 0;
		gbc_lblEggMoves.gridy = 2;
		panel_4.add(lblEggMoves, gbc_lblEggMoves);

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
		gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_3.gridx = 0;
		gbc_scrollPane_3.gridy = 3;
		panel_4.add(scrollPane_3, gbc_scrollPane_3);

		txaEggMoves = new JTextArea();
		scrollPane_3.setViewportView(txaEggMoves);

		JPanel panel_5 = new JPanel();
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.insets = new Insets(0, 0, 0, 5);
		gbc_panel_5.fill = GridBagConstraints.BOTH;
		gbc_panel_5.gridx = 2;
		gbc_panel_5.gridy = 1;
		frmPtaPokedexJedition.getContentPane().add(panel_5, gbc_panel_5);
		GridBagLayout gbl_panel_5 = new GridBagLayout();
		gbl_panel_5.columnWidths = new int[] { 0, 0 };
		gbl_panel_5.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_5.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_5.rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0,
				Double.MIN_VALUE };
		panel_5.setLayout(gbl_panel_5);

		JLabel lblEvolutionTree = new JLabel("Evolution Tree");
		GridBagConstraints gbc_lblEvolutionTree = new GridBagConstraints();
		gbc_lblEvolutionTree.insets = new Insets(0, 0, 5, 0);
		gbc_lblEvolutionTree.gridx = 0;
		gbc_lblEvolutionTree.gridy = 0;
		panel_5.add(lblEvolutionTree, gbc_lblEvolutionTree);

		JScrollPane scrollPane_4 = new JScrollPane();
		scrollPane_4
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane_4 = new GridBagConstraints();
		gbc_scrollPane_4.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_4.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_4.gridx = 0;
		gbc_scrollPane_4.gridy = 1;
		panel_5.add(scrollPane_4, gbc_scrollPane_4);

		txaEvoTree = new JTextArea();
		scrollPane_4.setViewportView(txaEvoTree);

		JLabel lblCapability = new JLabel("Capabilities");
		GridBagConstraints gbc_lblCapability = new GridBagConstraints();
		gbc_lblCapability.insets = new Insets(0, 0, 5, 0);
		gbc_lblCapability.gridx = 0;
		gbc_lblCapability.gridy = 2;
		panel_5.add(lblCapability, gbc_lblCapability);

		JScrollPane scrollPane_5 = new JScrollPane();
		scrollPane_5
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane_5 = new GridBagConstraints();
		gbc_scrollPane_5.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_5.gridx = 0;
		gbc_scrollPane_5.gridy = 3;
		panel_5.add(scrollPane_5, gbc_scrollPane_5);

		txaCapability = new JTextArea();
		scrollPane_5.setViewportView(txaCapability);

		JPanel panel_9 = new JPanel();
		GridBagConstraints gbc_panel_9 = new GridBagConstraints();
		gbc_panel_9.insets = new Insets(0, 0, 0, 5);
		gbc_panel_9.fill = GridBagConstraints.BOTH;
		gbc_panel_9.gridx = 3;
		gbc_panel_9.gridy = 1;
		frmPtaPokedexJedition.getContentPane().add(panel_9, gbc_panel_9);
		GridBagLayout gbl_panel_9 = new GridBagLayout();
		gbl_panel_9.columnWidths = new int[] { 0, 0 };
		gbl_panel_9.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_9.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_9.rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0,
				Double.MIN_VALUE };
		panel_9.setLayout(gbl_panel_9);

		JLabel label_1 = new JLabel("Level Up Moves");
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 0);
		gbc_label_1.gridx = 0;
		gbc_label_1.gridy = 0;
		panel_9.add(label_1, gbc_label_1);

		JScrollPane scrollPane_7 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_7 = new GridBagConstraints();
		gbc_scrollPane_7.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane_7.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_7.gridx = 0;
		gbc_scrollPane_7.gridy = 1;
		panel_9.add(scrollPane_7, gbc_scrollPane_7);
		scrollPane_7
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		txaLevelUpMoves = new JTextArea();
		scrollPane_7.setViewportView(txaLevelUpMoves);

		JLabel lblMoveTutor = new JLabel("Move Tutor");
		GridBagConstraints gbc_lblMoveTutor = new GridBagConstraints();
		gbc_lblMoveTutor.insets = new Insets(0, 0, 5, 0);
		gbc_lblMoveTutor.gridx = 0;
		gbc_lblMoveTutor.gridy = 2;
		panel_9.add(lblMoveTutor, gbc_lblMoveTutor);

		JScrollPane scrollPane_8 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_8 = new GridBagConstraints();
		gbc_scrollPane_8.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_8.gridx = 0;
		gbc_scrollPane_8.gridy = 3;
		panel_9.add(scrollPane_8, gbc_scrollPane_8);
		scrollPane_8
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		txaMoveTutor = new JTextArea();
		scrollPane_8.setViewportView(txaMoveTutor);

		JPanel panel_7 = new JPanel();
		GridBagConstraints gbc_panel_7 = new GridBagConstraints();
		gbc_panel_7.fill = GridBagConstraints.BOTH;
		gbc_panel_7.gridx = 4;
		gbc_panel_7.gridy = 1;
		frmPtaPokedexJedition.getContentPane().add(panel_7, gbc_panel_7);
		GridBagLayout gbl_panel_7 = new GridBagLayout();
		gbl_panel_7.columnWidths = new int[] { 0, 0 };
		gbl_panel_7.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_7.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_7.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel_7.setLayout(gbl_panel_7);

		updatePokemonDirectory();
	}

	private void loadPokemon(File file) {
		logger.info("Load pokemon from file [" + file.toString() + "]");
		Directory.mkdirs();
		Pokemon inVal = null;
		try {
			ObjectInputStream in = new ObjectInputStream(
					new BufferedInputStream(
							new FileInputStream(file.toString())));
			inVal = (Pokemon) in.readObject();

			lblProfile.setIcon(inVal.getProfile());
			lblProfile.setText("");
			txfName.setText(inVal.getName());
			cbxType1.setSelectedItem(inVal.getTYPE()[0]);
			cbxType2.setSelectedItem(inVal.getTYPE()[1]);
			txfHeight.setText(inVal.getHeight());
			txfWeight.setText(inVal.getWeight());
			txfGenderRatio.setText(inVal.getGenderRatio());
			txaAbilites.setText(inVal.getBasicAbilities());
			txaHighAbilities.setText(inVal.getHighAbilities());
			txaEvoTree.setText(inVal.getEvolution());
			txaCapability.setText(inVal.getCapabilities());
			txaHabitat.setText(inVal.getHabitat());
			cbxEggGroup1.setSelectedItem(inVal.getEggGroup()[0]);
			cbxEggGroup2.setSelectedItem(inVal.getEggGroup()[1]);
			cbxDiet.setSelectedItem(inVal.getDiet());
			spnrHatchRate.setValue(inVal.getHatchRate());
			spnrCatchRate.setValue(inVal.getCaptureRate());
			spnrExpDrop.setValue(inVal.getExperienceDrop());
			txaMoveTutor.setText(inVal.getMoveTutor());
			txaEggMoves.setText(inVal.getEggMoves());
			txaLevelUpMoves.setText(inVal.getLevelUpMoves());
			txaTMList.setText(inVal.getTMList());
			spnrAtk.setValue(inVal.getBaseAtk().getValue());
			spnrHP.setValue(inVal.getBaseHP().getValue());
			spnrDef.setValue(inVal.getBaseDef().getValue());
			spnrSAtk.setValue(inVal.getBaseSpAtk().getValue());
			spnrSDef.setValue(inVal.getBaseSpDef().getValue());
			spnrSpeed.setValue(inVal.getBaseSpeed().getValue());

			in.close();

			UpdateGenderRatio();
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(frmPtaPokedexJedition, e.toString(),
					"ClassNotFoundException", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			logger.info("Error CNFE while loading pokemon!", e);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(frmPtaPokedexJedition, e.toString(),
					"IOException", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			logger.info("Error IOE while loading pokemon!", e);
		} finally {
			System.out.println(Directory.toString());
			JOptionPane.showMessageDialog(frmPtaPokedexJedition, "Pokemon "
					+ inVal.getName() + " load without any issues!",
					"Pokemon Loaded", JOptionPane.INFORMATION_MESSAGE);
			logger.info("Pokemon loaded.");
		}

	}

	private void UpdateGenderRatio() {
		logger.info("Updating gender ratio info.");
		try {
			double male = 1, female = 1;
			male = Integer.parseInt(txfGenderRatio.getText().substring(0,
					txfGenderRatio.getText().indexOf(":")));
			female = Integer.parseInt(txfGenderRatio.getText().substring(
					txfGenderRatio.getText().indexOf(":") + 1));
			double mRate = male / (male + female) * 100, fRate = female
					/ (male + female) * 100;
			lblMaleFemale.setText("Male: " + mRate + "% / Female " + fRate
					+ "%");
		} catch (ArithmeticException e) {
			logger.log(Level.ERROR, "You managed to divid by zero! Congrats!",
					e);
		} finally {
			logger.info("Gender ratio info updated.");
		}
	}

	private void updatePokemonDirectory() {
		logger.log(Level.INFO, "Updating pokemon directory...");
		File[] files = Directory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".rpm");
			}
		});
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
		if (files != null)
			try {
				for (int i = 0; i < files.length; i++)
					model.addElement(files[i].getName().substring(0,
							files[i].getName().length() - 4));
			} catch (NullPointerException e) {
				logger.log(Level.ERROR, "Error while updating directory!", e);
				System.err.println("No files in directory!");
			} finally {
				lblPokemonCount.setText("Pokemon: " + files.length);
				cbxPokemonDatabase.setModel(model);
				logger.info("Pokemon directory updated.");
			}
	}

}
