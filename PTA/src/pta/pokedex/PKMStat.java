package pta.pokedex;

import java.io.Serializable;

public class PKMStat implements Serializable {

	private static final long serialVersionUID = -3735788150007166839L;
	private String name;
	private int value;

	public String getName() {
		return name;
	}

	public int getValue() {
		return value;
	}

	public PKMStat setName(String name) {
		this.name = name;
		return this;
	}

	public PKMStat setValue(int value) {
		this.value = value;
		return this;
	}

}
