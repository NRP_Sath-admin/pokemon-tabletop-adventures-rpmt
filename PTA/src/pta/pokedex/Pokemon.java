package pta.pokedex;

import java.io.Serializable;

import javax.swing.ImageIcon;

public class Pokemon implements Serializable {

	private static final long serialVersionUID = -4362836233912430698L;

	private ImageIcon profile;

	private PKMStat HP = new PKMStat().setName("HP");
	private PKMStat Atk = new PKMStat().setName("Atk");
	private PKMStat Def = new PKMStat().setName("Def");
	private PKMStat spAtk = new PKMStat().setName("SpAtk");
	private PKMStat spDef = new PKMStat().setName("SpDef");
	private PKMStat Speed = new PKMStat().setName("Speed");

	private int HatchRate;

	private String Name;
	private Type TYPE1 = Type.None;
	private Type TYPE2 = Type.None;
	private String BasicAbilities;
	private String HighAbilities;

	private String Evolution;

	private String Height;
	private String Weight;

	private String GenderRatio;
	private String EggGroup1;
	private String EggGroup2;

	private String Diet;
	private String Habitat;
	private String Capabilities;

	private int CaptureRate;
	private int ExperienceDrop;

	private String TMList;
	private String LevelUpMoves;
	private String MoveTutor;
	private String EggMoves;

	Pokemon() {

	}

	Pokemon(Pokemon pkm) {
		setProfile(pkm.profile);
		setAtk(pkm.getBaseAtk().getValue());
		setHP(pkm.getBaseHP().getValue());
		setDef(pkm.getBaseDef().getValue());
		setSpAtk(pkm.getBaseSpAtk().getValue());
		setSpDef(pkm.getBaseSpDef().getValue());
		setSpeed(pkm.getBaseSpeed().getValue());
		setName(pkm.getName());
		this.setTYPE(pkm.getTYPE()[0], pkm.getTYPE()[1]);
		setHeight(pkm.getHeight());
		setWeight(pkm.getWeight());
		setGenderRatio(pkm.getGenderRatio());
		setEvolution(pkm.getEvolution());
		setHighAbilities(pkm.getHighAbilities());
		setBasicAbilities(pkm.getBasicAbilities());
		setCapabilities(pkm.getCapabilities());
		setHabitat(pkm.getHabitat());
		this.setEggGroup(pkm.getEggGroup()[0], pkm.getEggGroup()[1]);
		setHatchRate(pkm.getHatchRate());
		setCaptureRate(pkm.getCaptureRate());
		setExperienceDrop(pkm.getExperienceDrop());
		setDiet(pkm.getDiet());
		setMoveTutor(pkm.getMoveTutor());
		setEggMoves(pkm.getEggMoves());
		setLevelUpMoves(pkm.getLevelUpMoves());
		setTMList(pkm.getTMList());
	}

	public PKMStat getBaseAtk() {
		return Atk;
	}

	public PKMStat getBaseDef() {
		return Def;
	}

	public PKMStat getBaseHP() {
		return HP;
	}

	public PKMStat getBaseSpAtk() {
		return spAtk;
	}

	public PKMStat getBaseSpDef() {
		return spDef;
	}

	public PKMStat getBaseSpeed() {
		return Speed;
	}

	public String getBasicAbilities() {
		return BasicAbilities;
	}

	public String getCapabilities() {
		return Capabilities;
	}

	public int getCaptureRate() {
		return CaptureRate;
	}

	public String getDiet() {
		return Diet;
	}

	public String[] getEggGroup() {
		String[] reVal = new String[2];
		if (EggGroup2 != null) {
			reVal[0] = EggGroup1;
			reVal[1] = EggGroup2;
		} else {
			reVal[0] = EggGroup1;
			reVal[1] = null;
		}
		return reVal;
	}

	public String getEggMoves() {
		return EggMoves;
	}

	public String getEvolution() {
		return Evolution;
	}

	public int getExperienceDrop() {
		return ExperienceDrop;
	}

	public String getGenderRatio() {
		return GenderRatio;
	}

	public String getHabitat() {
		return Habitat;
	}

	public int getHatchRate() {
		return HatchRate;
	}

	public String getHeight() {
		return Height;
	}

	public String getHighAbilities() {
		return HighAbilities;
	}

	public String getLevelUpMoves() {
		return LevelUpMoves;
	}

	public String getMoveTutor() {
		return MoveTutor;
	}

	public String getName() {
		return Name;
	}

	public ImageIcon getProfile() {
		return profile;
	}

	public String getTMList() {
		return TMList;
	}

	public Type[] getTYPE() {
		Type[] reVal = new Type[2];
		if (TYPE2.compareTo(Type.None) == 0) {
			reVal[0] = TYPE1;
			reVal[1] = TYPE2;
		} else {
			reVal[0] = TYPE1;
			reVal[1] = Type.None;
		}
		return reVal;
	}

	public String getWeight() {
		return Weight;
	}

	public void setAtk(int atk) {
		Atk.setValue(atk);
	}

	public void setBasicAbilities(String basicAbilities) {
		BasicAbilities = basicAbilities;
	}

	public void setCapabilities(String capabilities) {
		Capabilities = capabilities;
	}

	public void setCaptureRate(int captureRate) {
		CaptureRate = captureRate;
	}

	public void setDef(int def) {
		Def.setValue(def);
	}

	public void setDiet(String diet) {
		Diet = diet;
	}

	public void setEggGroup(String eggGroup1) {
		EggGroup1 = eggGroup1;
		EggGroup2 = null;
	}

	public void setEggGroup(String eggGroup1, String eggGroup2) {
		EggGroup1 = eggGroup1;
		EggGroup2 = eggGroup2;
	}

	public void setEggMoves(String eggMoves) {
		EggMoves = eggMoves;
	}

	public void setEvolution(String evolution) {
		Evolution = evolution;
	}

	public void setExperienceDrop(int experienceDrop) {
		ExperienceDrop = experienceDrop;
	}

	public void setGenderRatio(String genderRatio) {
		GenderRatio = genderRatio;
	}

	public void setHabitat(String habitat) {
		Habitat = habitat;
	}

	public void setHatchRate(int hatchrate) {
		HatchRate = hatchrate;
	}

	public void setHeight(String height) {
		Height = height;
	}

	public void setHighAbilities(String highAbilities) {
		HighAbilities = highAbilities;
	}

	public void setHP(int hP) {
		HP.setValue(hP);
	}

	public void setLevelUpMoves(String levelUpMoves) {
		LevelUpMoves = levelUpMoves;
	}

	public void setMoveTutor(String moveTutor) {
		MoveTutor = moveTutor;
	}

	public void setName(String name) {
		Name = name;
	}

	public void setProfile(ImageIcon icon) {
		profile = icon;
	}

	public void setSpAtk(int spAtk) {
		this.spAtk.setValue(spAtk);
	}

	public void setSpDef(int spDef) {
		this.spDef.setValue(spDef);
	}

	public void setSpeed(int speed) {
		Speed.setValue(speed);
	}

	public void setTMList(String tMList) {
		TMList = tMList;
	}

	public void setTYPE(Type type1) {
		TYPE1 = type1;
		TYPE2 = Type.None;
	}

	public void setTYPE(Type type1, Type type2) {
		TYPE1 = type1;
		TYPE2 = type2;
	}

	public void setWeight(String weight) {
		Weight = weight;
	}

	@Override
	public String toString() {
		return getName();
	}
}
