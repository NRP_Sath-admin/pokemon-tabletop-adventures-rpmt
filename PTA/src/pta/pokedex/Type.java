package pta.pokedex;

public enum Type {
	None(""), Normal("NORMAL"), Fight("FIGHT"), Flying("FLYING"), Poision(
			"POISON"), Ground("GROUND"), Rock("ROCK"), Bug("BUG"), Ghost(
			"GHOST"), Steel("STEEL"), Fire("FIRE"), Water("WATER"), Gress(
			"GRESS"), Electric("ELETRIC"), Psychic("PYSCHIC"), Ice("ICE"), Dragon(
			"DRAGON"), Dark("DARK"), Fairy("FAIRY");

	@SuppressWarnings("unused")
	private final String Name;

	Type(String name) {
		Name = name;
	}
}
