package pta.pokedex;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import pta.util.PokemonInfoPanel;
import javax.swing.JCheckBox;

public class TrainedPokemonManager {

	public class BaseRelationSorter implements Comparator<PKMStat> {
		@Override
		public int compare(PKMStat o1, PKMStat o2) {
			return Integer.compare(o1.getValue(), o2.getValue());
		}
	}

	private JFrame frmPokemonTrainer;
	private JTextField txfNickName;

	private TrainedPokemon pokemon = new TrainedPokemon();

	static final Logger logger = Logger.getLogger(PokedexWindow.class);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		BasicConfigurator.configure();

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();

		logger.setLevel(Level.ALL);
		logger.info("\n----------" + dateFormat.format(date) + "----------\n"
				+ "Starting Application...\n");

		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					TrainedPokemonManager window = new TrainedPokemonManager();
					window.frmPokemonTrainer.setVisible(true);
				} catch (Exception e) {
					logger.log(Level.FATAL, e);
					e.printStackTrace();
				}
			}
		});
	}

	private final File Directory = new File(System.getProperty("user.home")
			+ "\\.pta\\pokedex\\database\\");

	private JLabel lblBaseAtk;

	private JLabel lblHPBase;

	private JLabel lblBaseDef;

	private JLabel lblBaseSpAtk;
	private JLabel lblBaseSpDef;
	private JLabel lblBaseSpeed;
	private JSpinner spnrHP;
	private JSpinner spnrAtk;
	private JSpinner spnrDef;
	private JSpinner spnrSpAtk;
	private JSpinner spnrSpDef;
	private JSpinner spnrSpeed;
	private JLabel lblHPFinal;
	private JLabel lblAtkFinal;
	private JLabel lblDefFinal;
	private JLabel lblSpAtkFinal;
	private JLabel lblSpDefFinal;
	private JLabel lblSpeedFinal;
	private JTextArea txaNotes;
	private JLabel lblProfile;
	private ArrayList<PKMStat> baseRelation;
	private JComboBox<Pokemon> cbxSpecies;
	private PokemonInfoPanel pokemonInfoPanel;
	private DefaultComboBoxModel<String> learnableMoves = new DefaultComboBoxModel<String>();
	private JComboBox<String> cbxMoves;
	private DefaultListModel dlmKnownMoves = new DefaultListModel();
	private JList<String> listKnownMoves;
	private JCheckBox chckbxNewCheckBox;

	/**
	 * Create the application.
	 */
	public TrainedPokemonManager() {
		initialize();
	}

	public TrainedPokemonManager(TrainedPokemon tp) {
		initialize();
		loadTrainedPokemon(tp);
	}
	
	private void loadTrainedPokemon(TrainedPokemon tp) {
		pokemon = tp;
		updateInfo();
	}
	
	private void loadTrainedPokemon(File file) {
		//Load trained pokemon file then direct to loadTrainedPokemon(TrainedPokemon tp)
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		try {
			// Set System L&F
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (UnsupportedLookAndFeelException e) {
			logger.log(Level.ERROR, e);
		} catch (ClassNotFoundException e) {
			logger.log(Level.ERROR, e);
		} catch (InstantiationException e) {
			logger.log(Level.ERROR, e);
		} catch (IllegalAccessException e) {
			logger.log(Level.ERROR, e);
		}

		frmPokemonTrainer = new JFrame();
		frmPokemonTrainer.setResizable(false);
		frmPokemonTrainer.setIconImage(Toolkit.getDefaultToolkit().getImage(
				TrainedPokemonManager.class
						.getResource("/pta/img/32_pokeball.png")));
		frmPokemonTrainer.setTitle("Pokemon Trainer");
		frmPokemonTrainer.setBounds(100, 100, 686, 545);
		frmPokemonTrainer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 255, 246, 147, 0 };
		gridBagLayout.rowHeights = new int[] { 255, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 1.0,
				Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		frmPokemonTrainer.getContentPane().setLayout(gridBagLayout);

		JPanel panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		frmPokemonTrainer.getContentPane().add(panel, gbc_panel);

		lblProfile = new JLabel("<img>");
		lblProfile.setHorizontalAlignment(SwingConstants.CENTER);
		panel.add(lblProfile);

		JPanel panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 5);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 1;
		gbc_panel_3.gridy = 0;
		frmPokemonTrainer.getContentPane().add(panel_3, gbc_panel_3);
		GridBagLayout gbl_panel_3 = new GridBagLayout();
		gbl_panel_3.columnWidths = new int[] { 0, 0 };
		gbl_panel_3.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_3.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_3.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel_3.setLayout(gbl_panel_3);

		JLabel lblPokemonInfo = new JLabel("Pokemon Info");
		GridBagConstraints gbc_lblPokemonInfo = new GridBagConstraints();
		gbc_lblPokemonInfo.insets = new Insets(0, 0, 5, 0);
		gbc_lblPokemonInfo.gridx = 0;
		gbc_lblPokemonInfo.gridy = 0;
		panel_3.add(lblPokemonInfo, gbc_lblPokemonInfo);

		pokemonInfoPanel = new PokemonInfoPanel();
		GridBagConstraints gbc_pokemonInfoPanel = new GridBagConstraints();
		gbc_pokemonInfoPanel.fill = GridBagConstraints.BOTH;
		gbc_pokemonInfoPanel.gridx = 0;
		gbc_pokemonInfoPanel.gridy = 1;
		panel_3.add(pokemonInfoPanel, gbc_pokemonInfoPanel);

		JPanel panel_1 = new JPanel();
		GridBagConstraints gbc_panel_1 = new GridBagConstraints();
		gbc_panel_1.insets = new Insets(0, 0, 5, 0);
		gbc_panel_1.fill = GridBagConstraints.BOTH;
		gbc_panel_1.gridx = 2;
		gbc_panel_1.gridy = 0;
		frmPokemonTrainer.getContentPane().add(panel_1, gbc_panel_1);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0,
				Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				0.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JLabel lblStat = new JLabel("Stat");
		GridBagConstraints gbc_lblStat = new GridBagConstraints();
		gbc_lblStat.anchor = GridBagConstraints.ABOVE_BASELINE;
		gbc_lblStat.insets = new Insets(0, 0, 5, 5);
		gbc_lblStat.gridx = 0;
		gbc_lblStat.gridy = 0;
		panel_1.add(lblStat, gbc_lblStat);

		JLabel lblBase = new JLabel("Base");
		GridBagConstraints gbc_lblBase = new GridBagConstraints();
		gbc_lblBase.anchor = GridBagConstraints.ABOVE_BASELINE;
		gbc_lblBase.insets = new Insets(0, 0, 5, 5);
		gbc_lblBase.gridx = 1;
		gbc_lblBase.gridy = 0;
		panel_1.add(lblBase, gbc_lblBase);

		JLabel lblAdded = new JLabel("Added");
		GridBagConstraints gbc_lblAdded = new GridBagConstraints();
		gbc_lblAdded.anchor = GridBagConstraints.ABOVE_BASELINE;
		gbc_lblAdded.insets = new Insets(0, 0, 5, 5);
		gbc_lblAdded.gridx = 2;
		gbc_lblAdded.gridy = 0;
		panel_1.add(lblAdded, gbc_lblAdded);

		JLabel lblFinal = new JLabel("Final");
		GridBagConstraints gbc_lblFinal = new GridBagConstraints();
		gbc_lblFinal.anchor = GridBagConstraints.ABOVE_BASELINE;
		gbc_lblFinal.insets = new Insets(0, 0, 5, 0);
		gbc_lblFinal.gridx = 3;
		gbc_lblFinal.gridy = 0;
		panel_1.add(lblFinal, gbc_lblFinal);

		JLabel lblHp = new JLabel("HP");
		GridBagConstraints gbc_lblHp = new GridBagConstraints();
		gbc_lblHp.insets = new Insets(0, 0, 5, 5);
		gbc_lblHp.gridx = 0;
		gbc_lblHp.gridy = 1;
		panel_1.add(lblHp, gbc_lblHp);

		lblHPBase = new JLabel("0");
		GridBagConstraints gbc_lblHPBase = new GridBagConstraints();
		gbc_lblHPBase.insets = new Insets(0, 0, 5, 5);
		gbc_lblHPBase.gridx = 1;
		gbc_lblHPBase.gridy = 1;
		panel_1.add(lblHPBase, gbc_lblHPBase);

		spnrHP = new JSpinner();
		spnrHP.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {

				if (true/* Place holder for base relation check */)
					pokemon.setBonusHP((Integer) spnrHP.getValue());

				updateFinalStats();
			}
		});
		GridBagConstraints gbc_spnrHP = new GridBagConstraints();
		gbc_spnrHP.insets = new Insets(0, 0, 5, 5);
		gbc_spnrHP.gridx = 2;
		gbc_spnrHP.gridy = 1;
		panel_1.add(spnrHP, gbc_spnrHP);

		lblHPFinal = new JLabel("0");
		GridBagConstraints gbc_lblHPFinal = new GridBagConstraints();
		gbc_lblHPFinal.insets = new Insets(0, 0, 5, 0);
		gbc_lblHPFinal.gridx = 3;
		gbc_lblHPFinal.gridy = 1;
		panel_1.add(lblHPFinal, gbc_lblHPFinal);

		JLabel lblAtk = new JLabel("Atk");
		GridBagConstraints gbc_lblAtk = new GridBagConstraints();
		gbc_lblAtk.insets = new Insets(0, 0, 5, 5);
		gbc_lblAtk.gridx = 0;
		gbc_lblAtk.gridy = 2;
		panel_1.add(lblAtk, gbc_lblAtk);

		lblBaseAtk = new JLabel("0");
		GridBagConstraints gbc_lblBaseAtk = new GridBagConstraints();
		gbc_lblBaseAtk.insets = new Insets(0, 0, 5, 5);
		gbc_lblBaseAtk.gridx = 1;
		gbc_lblBaseAtk.gridy = 2;
		panel_1.add(lblBaseAtk, gbc_lblBaseAtk);

		spnrAtk = new JSpinner();
		spnrAtk.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {

				if (true/* Place holder for base relation check */)
					pokemon.setBonusAtk((Integer) spnrAtk.getValue());

				updateFinalStats();
			}
		});
		GridBagConstraints gbc_spnrAtk = new GridBagConstraints();
		gbc_spnrAtk.insets = new Insets(0, 0, 5, 5);
		gbc_spnrAtk.gridx = 2;
		gbc_spnrAtk.gridy = 2;
		panel_1.add(spnrAtk, gbc_spnrAtk);

		lblAtkFinal = new JLabel("0");
		GridBagConstraints gbc_lblAtkFinal = new GridBagConstraints();
		gbc_lblAtkFinal.insets = new Insets(0, 0, 5, 0);
		gbc_lblAtkFinal.gridx = 3;
		gbc_lblAtkFinal.gridy = 2;
		panel_1.add(lblAtkFinal, gbc_lblAtkFinal);

		JLabel lblDef = new JLabel("Def");
		GridBagConstraints gbc_lblDef = new GridBagConstraints();
		gbc_lblDef.insets = new Insets(0, 0, 5, 5);
		gbc_lblDef.gridx = 0;
		gbc_lblDef.gridy = 3;
		panel_1.add(lblDef, gbc_lblDef);

		lblBaseDef = new JLabel("0");
		GridBagConstraints gbc_lblBaseDef = new GridBagConstraints();
		gbc_lblBaseDef.insets = new Insets(0, 0, 5, 5);
		gbc_lblBaseDef.gridx = 1;
		gbc_lblBaseDef.gridy = 3;
		panel_1.add(lblBaseDef, gbc_lblBaseDef);

		spnrDef = new JSpinner();
		spnrDef.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {

				if (true/* Place holder for base relation check */)
					pokemon.setBonusDef((Integer) spnrDef.getValue());

				updateFinalStats();
			}
		});
		GridBagConstraints gbc_spnrDef = new GridBagConstraints();
		gbc_spnrDef.insets = new Insets(0, 0, 5, 5);
		gbc_spnrDef.gridx = 2;
		gbc_spnrDef.gridy = 3;
		panel_1.add(spnrDef, gbc_spnrDef);

		lblDefFinal = new JLabel("0");
		GridBagConstraints gbc_lblDefFinal = new GridBagConstraints();
		gbc_lblDefFinal.insets = new Insets(0, 0, 5, 0);
		gbc_lblDefFinal.gridx = 3;
		gbc_lblDefFinal.gridy = 3;
		panel_1.add(lblDefFinal, gbc_lblDefFinal);

		JLabel lblSpAtk = new JLabel("Sp. Atk");
		GridBagConstraints gbc_lblSpAtk = new GridBagConstraints();
		gbc_lblSpAtk.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpAtk.gridx = 0;
		gbc_lblSpAtk.gridy = 4;
		panel_1.add(lblSpAtk, gbc_lblSpAtk);

		lblBaseSpAtk = new JLabel("0");
		GridBagConstraints gbc_lblBaseSpAtk = new GridBagConstraints();
		gbc_lblBaseSpAtk.insets = new Insets(0, 0, 5, 5);
		gbc_lblBaseSpAtk.gridx = 1;
		gbc_lblBaseSpAtk.gridy = 4;
		panel_1.add(lblBaseSpAtk, gbc_lblBaseSpAtk);

		spnrSpAtk = new JSpinner();
		spnrSpAtk.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {

				if (true/* Place holder for base relation check */)
					pokemon.setBonusSpAtk((Integer) spnrSpAtk.getValue());

				updateFinalStats();
			}
		});
		GridBagConstraints gbc_spnrSpAtk = new GridBagConstraints();
		gbc_spnrSpAtk.insets = new Insets(0, 0, 5, 5);
		gbc_spnrSpAtk.gridx = 2;
		gbc_spnrSpAtk.gridy = 4;
		panel_1.add(spnrSpAtk, gbc_spnrSpAtk);

		lblSpAtkFinal = new JLabel("0");
		GridBagConstraints gbc_lblSpAtkFinal = new GridBagConstraints();
		gbc_lblSpAtkFinal.insets = new Insets(0, 0, 5, 0);
		gbc_lblSpAtkFinal.gridx = 3;
		gbc_lblSpAtkFinal.gridy = 4;
		panel_1.add(lblSpAtkFinal, gbc_lblSpAtkFinal);

		JLabel lblSpDef = new JLabel("Sp. Def");
		GridBagConstraints gbc_lblSpDef = new GridBagConstraints();
		gbc_lblSpDef.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpDef.gridx = 0;
		gbc_lblSpDef.gridy = 5;
		panel_1.add(lblSpDef, gbc_lblSpDef);

		lblBaseSpDef = new JLabel("0");
		GridBagConstraints gbc_lblBaseSpDef = new GridBagConstraints();
		gbc_lblBaseSpDef.insets = new Insets(0, 0, 5, 5);
		gbc_lblBaseSpDef.gridx = 1;
		gbc_lblBaseSpDef.gridy = 5;
		panel_1.add(lblBaseSpDef, gbc_lblBaseSpDef);

		spnrSpDef = new JSpinner();
		spnrSpDef.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {

				if (true/* Place holder for base relation check */)
					pokemon.setBonusSpDef((Integer) spnrSpDef.getValue());

				updateFinalStats();
			}
		});
		GridBagConstraints gbc_spnrSpDef = new GridBagConstraints();
		gbc_spnrSpDef.insets = new Insets(0, 0, 5, 5);
		gbc_spnrSpDef.gridx = 2;
		gbc_spnrSpDef.gridy = 5;
		panel_1.add(spnrSpDef, gbc_spnrSpDef);

		lblSpDefFinal = new JLabel("0");
		GridBagConstraints gbc_lblSpDefFinal = new GridBagConstraints();
		gbc_lblSpDefFinal.insets = new Insets(0, 0, 5, 0);
		gbc_lblSpDefFinal.gridx = 3;
		gbc_lblSpDefFinal.gridy = 5;
		panel_1.add(lblSpDefFinal, gbc_lblSpDefFinal);

		JLabel lblSpeed = new JLabel("Speed");
		GridBagConstraints gbc_lblSpeed = new GridBagConstraints();
		gbc_lblSpeed.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpeed.gridx = 0;
		gbc_lblSpeed.gridy = 6;
		panel_1.add(lblSpeed, gbc_lblSpeed);

		lblBaseSpeed = new JLabel("0");
		GridBagConstraints gbc_lblBaseSpeed = new GridBagConstraints();
		gbc_lblBaseSpeed.insets = new Insets(0, 0, 5, 5);
		gbc_lblBaseSpeed.gridx = 1;
		gbc_lblBaseSpeed.gridy = 6;
		panel_1.add(lblBaseSpeed, gbc_lblBaseSpeed);

		spnrSpeed = new JSpinner();
		spnrSpeed.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent arg0) {

				if (true/* Place holder for base relation check */)
					pokemon.setBonusSpeed((Integer) spnrSpeed.getValue());

				updateFinalStats();
			}
		});
		GridBagConstraints gbc_spnrSpeed = new GridBagConstraints();
		gbc_spnrSpeed.insets = new Insets(0, 0, 5, 5);
		gbc_spnrSpeed.gridx = 2;
		gbc_spnrSpeed.gridy = 6;
		panel_1.add(spnrSpeed, gbc_spnrSpeed);

		lblSpeedFinal = new JLabel("0");
		GridBagConstraints gbc_lblSpeedFinal = new GridBagConstraints();
		gbc_lblSpeedFinal.insets = new Insets(0, 0, 5, 0);
		gbc_lblSpeedFinal.gridx = 3;
		gbc_lblSpeedFinal.gridy = 6;
		panel_1.add(lblSpeedFinal, gbc_lblSpeedFinal);
		
		chckbxNewCheckBox = new JCheckBox("Stat Unlock");
		chckbxNewCheckBox.setToolTipText("This is currently disabled you will have to watch your base relation manually, sorry.");
		chckbxNewCheckBox.setEnabled(false);
		chckbxNewCheckBox.setSelected(true);
		GridBagConstraints gbc_chckbxNewCheckBox = new GridBagConstraints();
		gbc_chckbxNewCheckBox.insets = new Insets(0, 0, 0, 5);
		gbc_chckbxNewCheckBox.gridx = 0;
		gbc_chckbxNewCheckBox.gridy = 7;
		panel_1.add(chckbxNewCheckBox, gbc_chckbxNewCheckBox);

		JPanel panel_2 = new JPanel();
		GridBagConstraints gbc_panel_2 = new GridBagConstraints();
		gbc_panel_2.insets = new Insets(0, 0, 0, 5);
		gbc_panel_2.fill = GridBagConstraints.BOTH;
		gbc_panel_2.gridx = 0;
		gbc_panel_2.gridy = 1;
		frmPokemonTrainer.getContentPane().add(panel_2, gbc_panel_2);
		GridBagLayout gbl_panel_2 = new GridBagLayout();
		gbl_panel_2.columnWidths = new int[] { 58, 0, 0 };
		gbl_panel_2.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_panel_2.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gbl_panel_2.rowWeights = new double[] { 0.0, 0.0, 1.0, Double.MIN_VALUE };
		panel_2.setLayout(gbl_panel_2);

		JLabel lblName = new JLabel("Name");
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 0;
		gbc_lblName.gridy = 0;
		panel_2.add(lblName, gbc_lblName);

		txfNickName = new JTextField();
		GridBagConstraints gbc_txfNickName = new GridBagConstraints();
		gbc_txfNickName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txfNickName.insets = new Insets(0, 0, 5, 0);
		gbc_txfNickName.gridx = 1;
		gbc_txfNickName.gridy = 0;
		panel_2.add(txfNickName, gbc_txfNickName);
		txfNickName.setColumns(10);

		JLabel lblSpecies = new JLabel("Species");
		GridBagConstraints gbc_lblSpecies = new GridBagConstraints();
		gbc_lblSpecies.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpecies.gridx = 0;
		gbc_lblSpecies.gridy = 1;
		panel_2.add(lblSpecies, gbc_lblSpecies);

		cbxSpecies = new JComboBox<Pokemon>();
		cbxSpecies.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				loadPokemon("Riolu");
			}
		});
		GridBagConstraints gbc_cbxSpecies = new GridBagConstraints();
		gbc_cbxSpecies.insets = new Insets(0, 0, 5, 0);
		gbc_cbxSpecies.fill = GridBagConstraints.HORIZONTAL;
		gbc_cbxSpecies.gridx = 1;
		gbc_cbxSpecies.gridy = 1;
		panel_2.add(cbxSpecies, gbc_cbxSpecies);

		JLabel lblMoves = new JLabel("Moves");
		GridBagConstraints gbc_lblMoves = new GridBagConstraints();
		gbc_lblMoves.anchor = GridBagConstraints.NORTH;
		gbc_lblMoves.insets = new Insets(0, 0, 0, 5);
		gbc_lblMoves.gridx = 0;
		gbc_lblMoves.gridy = 2;
		panel_2.add(lblMoves, gbc_lblMoves);

		JPanel scrollPane_1 = new JPanel();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 1;
		gbc_scrollPane_1.gridy = 2;
		panel_2.add(scrollPane_1, gbc_scrollPane_1);
		scrollPane_1.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane_3 = new JScrollPane();
		scrollPane_3
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane_1.add(scrollPane_3, BorderLayout.CENTER);

		listKnownMoves = new JList<String>();
		scrollPane_3.setViewportView(listKnownMoves);

		JPanel panel_5 = new JPanel();
		scrollPane_1.add(panel_5, BorderLayout.SOUTH);
		panel_5.setLayout(new BorderLayout(0, 0));

		JButton btnNewButton = new JButton("Add");
		btnNewButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				dlmKnownMoves.addElement(cbxMoves.getSelectedItem());
				listKnownMoves.setModel(dlmKnownMoves);
			}
		});
		panel_5.add(btnNewButton, BorderLayout.CENTER);

		cbxMoves = new JComboBox<String>();
		panel_5.add(cbxMoves, BorderLayout.NORTH);

		JButton btnRemove = new JButton("Remove");
		btnRemove.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (!listKnownMoves.isSelectionEmpty())
					dlmKnownMoves.remove(listKnownMoves.getSelectedIndex());
			}
		});
		panel_5.add(btnRemove, BorderLayout.EAST);

		JPanel panel_4 = new JPanel();
		GridBagConstraints gbc_panel_4 = new GridBagConstraints();
		gbc_panel_4.insets = new Insets(0, 0, 0, 5);
		gbc_panel_4.fill = GridBagConstraints.BOTH;
		gbc_panel_4.gridx = 1;
		gbc_panel_4.gridy = 1;
		frmPokemonTrainer.getContentPane().add(panel_4, gbc_panel_4);
		GridBagLayout gbl_panel_4 = new GridBagLayout();
		gbl_panel_4.columnWidths = new int[] { 0, 0 };
		gbl_panel_4.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel_4.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
		gbl_panel_4.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel_4.setLayout(gbl_panel_4);

		JLabel lblNotes = new JLabel("Notes");
		GridBagConstraints gbc_lblNotes = new GridBagConstraints();
		gbc_lblNotes.insets = new Insets(0, 0, 5, 0);
		gbc_lblNotes.gridx = 0;
		gbc_lblNotes.gridy = 0;
		panel_4.add(lblNotes, gbc_lblNotes);

		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2
				.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.gridx = 0;
		gbc_scrollPane_2.gridy = 1;
		panel_4.add(scrollPane_2, gbc_scrollPane_2);

		txaNotes = new JTextArea();
		txaNotes.setWrapStyleWord(true);
		txaNotes.setLineWrap(true);
		scrollPane_2.setViewportView(txaNotes);

		JMenuBar menuBar = new JMenuBar();
		frmPokemonTrainer.setJMenuBar(menuBar);

		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);

		JMenuItem mntmSave = new JMenuItem("Save");
		mntmSave.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				savePokemon();
			}
		});
		mnFile.add(mntmSave);

		JMenuItem mntmLoad = new JMenuItem("Load");
		mnFile.add(mntmLoad);

		loadPokemonDatabase();
	}

	public boolean isNewLine(char[] charToCheck, int index) {
		if (charToCheck == null || charToCheck.length == 0)
			return false;
		else if (charToCheck[index] == '\n' || charToCheck[index] == '\r')
			return true;
		else
			return false;
	}

	private void loadPokemon(String name) {
		logger.log(Level.INFO, "loading pokemon " + name);
		if (Directory.exists())
			logger.log(Level.INFO, "Directory exists.");
		else {
			logger.log(Level.INFO, "Creating directory.");
			Directory.mkdirs();
		}
		try {
			ObjectInputStream in = new ObjectInputStream(
					new BufferedInputStream(new FileInputStream(Directory
							.toString() + "\\" + name + ".rpm")));
			pokemon = new TrainedPokemon((Pokemon) in.readObject());
			in.close();
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(frmPokemonTrainer, e.toString(),
					"ClassNotFoundException", JOptionPane.ERROR_MESSAGE);
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(frmPokemonTrainer, e.toString(),
					"IOException", JOptionPane.ERROR_MESSAGE);
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		}
		logger.log(Level.INFO, "Pokemon " + name + " loaded.");
		System.out.println(Directory.toString());
		updateInfo();
	}

	private void loadPokemonDatabase() {
		logger.log(Level.INFO, "Loading pokemon database...");
		File[] files = Directory.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.endsWith(".rpm");
			}
		});
		DefaultComboBoxModel<Pokemon> temp = new DefaultComboBoxModel<Pokemon>();
		try {
			for (int i = 0; i < files.length; i++) {
				logger.log(Level.INFO, "Loading pokemon: " + files[i].getName());
				ObjectInputStream in = new ObjectInputStream(
						new BufferedInputStream(new FileInputStream(
								files[i].toString())));
				temp.addElement((Pokemon) in.readObject());
				in.close();
			}
		} catch (NullPointerException e) {
			System.err.println("No files in directory!");
			logger.log(Level.ERROR, e);
		} catch (FileNotFoundException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (IOException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			logger.log(Level.ERROR, e);
			e.printStackTrace();
		} finally {
			logger.log(Level.INFO, "Pokemon database loaded.");
			cbxSpecies.setModel(temp);
		}
	}

	public void savePokemon() {
		File tempDir = new File(Directory.toString() + "\\Trained Pokemon");
		pokemon.setNotes(txaNotes.getText());
		pokemon.setNickName(txfNickName.getText());
		pokemon.setLevel(pokemonInfoPanel.getLevel());
		pokemon.setExp(pokemonInfoPanel.getEXP());
		pokemon.setGender(pokemonInfoPanel.getGender());
		System.out.println(pokemon.getGender());
		try {
			if (tempDir.exists())
				logger.info("Directory exists, no need to make it again.");
			else {
				logger.info("Creating directory " + tempDir.toString());
				tempDir.mkdirs();
			}
			logger.info("Initiating ObjectIutputStream.");
			ObjectOutputStream out = new ObjectOutputStream(
					new BufferedOutputStream(new FileOutputStream(
							tempDir.toString() + "\\" + pokemon.getName() + "("
									+ pokemon.getNickName() + ").rpm", false)));
			logger.info("Writting save data to file.");
			out.writeObject(pokemon);
			out.close();
			JOptionPane.showMessageDialog(frmPokemonTrainer, "Pokemon "
					+ pokemon.getNickName() + " saved without any issues!",
					"Pokemon Saved", JOptionPane.INFORMATION_MESSAGE);
		} catch (IOException e) {
			logger.log(Level.ERROR, "Error while saving!", e);
			JOptionPane.showMessageDialog(frmPokemonTrainer, e.toString(),
					"IOException", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	private void setUpBaseRelation() {
		logger.log(Level.INFO, "Calculating base relation");
		baseRelation = new ArrayList<PKMStat>();

		baseRelation.add(pokemon.getBaseHP());
		baseRelation.add(pokemon.getBaseAtk());
		baseRelation.add(pokemon.getBaseDef());
		baseRelation.add(pokemon.getBaseSpAtk());
		baseRelation.add(pokemon.getBaseSpDef());
		baseRelation.add(pokemon.getBaseSpeed());

		logger.log(Level.DEBUG, "Starting sorting " + baseRelation.toString());
		Collections.sort(baseRelation, new BaseRelationSorter());
		// Sorted biggest last.
		logger.log(Level.DEBUG, "Sorting finished! " + baseRelation.toString());
		logger.log(Level.INFO, "Base relation calculated, sorting finished.");
	}

	private void updateFinalStats() {
		logger.log(Level.INFO, "Updating final stat labels.");
		lblHPFinal.setText(""
				+ (pokemon.getBaseHP().getValue() + pokemon.getBonusHP()));
		lblAtkFinal.setText(""
				+ (pokemon.getBaseAtk().getValue() + pokemon.getBonusAtk()));
		lblDefFinal.setText(""
				+ (pokemon.getBaseDef().getValue() + pokemon.getBonusDef()));
		lblSpAtkFinal
				.setText(""
						+ (pokemon.getBaseSpAtk().getValue() + pokemon
								.getBonusSpAtk()));
		lblSpDefFinal
				.setText(""
						+ (pokemon.getBaseSpDef().getValue() + pokemon
								.getBonusSpDef()));
		lblSpeedFinal
				.setText(""
						+ (pokemon.getBaseSpeed().getValue() + pokemon
								.getBonusSpeed()));
		logger.log(Level.INFO, "Final stat labels updated.");
	}

	private void updateInfo() {
		logger.log(Level.INFO, "Updating info.");
		for (int i = 0; i < cbxSpecies.getItemCount(); i++)
			if (cbxSpecies.getItemAt(i).getName()
					.equalsIgnoreCase(pokemon.getName()))
				cbxSpecies.setSelectedIndex(i);
		logger.log(Level.DEBUG, "HP:" + pokemon.getBaseHP().getValue()
				+ "\nAtk:" + pokemon.getBaseAtk().getValue() + "\nDef:"
				+ pokemon.getBaseDef().getValue() + "\nSpAtk:"
				+ pokemon.getBaseSpAtk().getValue() + "\nSpDef:"
				+ pokemon.getBaseSpDef().getValue() + "\nSpeed:"
				+ pokemon.getBaseSpeed().getValue());

		lblHPBase.setText("" + pokemon.getBaseHP().getValue());
		lblBaseAtk.setText("" + pokemon.getBaseAtk().getValue());
		lblBaseDef.setText("" + pokemon.getBaseDef().getValue());
		lblBaseSpAtk.setText("" + pokemon.getBaseSpAtk().getValue());
		lblBaseSpDef.setText("" + pokemon.getBaseSpDef().getValue());
		lblBaseSpeed.setText("" + pokemon.getBaseSpeed().getValue());

		updateFinalStats();

		pokemonInfoPanel.updateInfo(pokemon);
		// Load Possible Moves
		learnableMoves.removeAllElements();
		String val = pokemon.getEggMoves() + "\n" + pokemon.getLevelUpMoves()
				+ "\n" + pokemon.getMoveTutor() + "\n" + pokemon.getTMList();
		int holder = 0;
		for (int i = 0; i < val.length(); i++)
			if (isNewLine(val.toCharArray(), i)) {
				learnableMoves.addElement(val.substring(holder, i));
				holder = i + 1;
			}
		cbxMoves.setModel(learnableMoves);
		setUpBaseRelation();
		logger.log(Level.INFO, "Info updated.");
	}
}