Pokemon Tabletop Adventures(PTA) RPMT(Role Play Managment Tools) is a series of (at the time of writing) three programs which are being designed to make running and playing Pokemon Tabletop Adventures simpler.

See the [wiki](https://bitbucket.org/NRP_Sath-admin/pokemon-tabletop-adventures-rpmt/wiki/Features) for planned features and updates.

For more info on Pokemon table top adventures check out these links.

[PTA Wiki](http://pokemontabletop.wikidot.com/)
[PTA Fourm](http://s4.zetaboards.com/Pokemon_Tabletop/forum/3052533/)

***

# Important Info About the PTA RMPT Programs #
Pokemon data is not packaged with the programs currently, if you want to take the time to make some of the pokemon using the pokemon creator (Pokedex J Edition) feel free but keep in mind you're files may become unusable as versions progress, I'll do my best to prevent that and make converters.
At some point I would like to include the pokemon data specifically from the PTA Wiki.

# Helping With Development #
I would love help, maintaining a program by yourself isn't easy.  Make a fork and make the additions or changes you want and submit a pull request.  As long as it's resonable to pull then I'll do it.  Now making forks and pull requests can become tedious so after I feel you have made enough contributions I'll give you write access.

## Things you can do to Help ##

* Fix bugs in the issue tracker.
* Submit bugs in the issue tracker.
* Fixes any code that is redundent, unnessacary, or bad practice.

## Things you Shouldn't do to Help ##
These mostly apply to people who don't have write access.

* Fixs typos(Just submit a bug about it, no need to make a fork for a typo).
* Add pokemon data.  Seriously don't, the pokemon object **IS** subject to change!
* Making a minor change for the sake of saying you made a contribution.  Now minor dosen't mean "one line of code" that one line may have fixed every bug in the program, minor means adding this line, "//I HELPED LOLZ! system.out.println("UNESSACARY DEBUG!");"